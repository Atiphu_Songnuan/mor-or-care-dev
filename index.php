<?php
define('SECURITY', 1);
define("ROOTPATH", realpath(dirname(__FILE__)));
date_default_timezone_set('Asia/Bangkok');
//error_reporting( error_reporting() & ~E_NOTICE );
//ini_set('display_errors', 1);
error_reporting(error_reporting() & ~E_ERROR & ~E_WARNING & ~E_PARSE & ~E_NOTICE);
require_once "config.php";
require_once "model/autoload.php";

//require class
$c_sry = new class_security();
$c_sql = new class_mysql();
$c_file = new class_files();
$c_fun = new class_functions();

//accept ip
// if (!in_array($c_sry->getip(), $CONFIGS['accept_ip'])) {
//     header("HTTP/1.1 404 Not Found");
//     die;
// }

//setup
$isProduction = $CONFIGS['mode'] == 'product' ? true : false;
$c_sql->debug = false;
$c_sry->https = true;
$c_sry->rewrite = false;
$c_sry->jwt = new jwt();
$c_sry->jwt->secret = $CONFIGS['jwt']['secret'];
$get = $c_sry->GET_VALUE(basename(__FILE__));
$post = $c_sry->POST_VALUE();

//check authorize
$is_token = false;
$access = null;
$authorize = $c_sry->get_token();

if (isset($authorize)) {
    $access = $authorize;
    $hn = $authorize['hn'];
    if (isset($authorize['exp'])) {
        $exp = $authorize['exp'];
        if (time() > intval($exp)) {
            header("HTTP/1.1 401 Unauthorized");die;
        }
    }
    $is_token = true;
}

$sprint = array(
    "verify4Signup" => "sprint1",
    "smsService" => "sprint1",
    "patientHosAppSignup" => "sprint1",
    "token" => "sprint1",
    "patientProfile" => "sprint1",
    "medicalWelfares" => "sprint2",
    "sendAppointmentNotification" => "sprint2",
    "appointmentList" => "sprint2",
    "appointmentDetail" => "sprint2",
    "checkInStatus" => "sprint2",
    "sendMasterDataClinicCode" => "sprint2",
    "drugAllergy" => "sprint3",
    "lastPrescription" => "sprint3",
    "drugHistory" => "sprint3",
    "drugImage" => "sprint3",
    "requestPaymentNotification" => "sprint4",
    "paymentList" => "sprint4",
    "paymentDetail" => "sprint4",
    "verifyPayment" => "sprint4",
    "setPayment" => "sprint4",
    "unlockPayment" => "sprint4",
    "sentVoidPayment" => "sprint4",
    "Authentication" => "sprint4",
    "sendQueueNotification" => "sprint4",
    "getQueueInfo" => "sprint4",
    "getJourneyStep" => "sprint5",
    "genTokenSCB" => "other",
    "debug" => "other",
);

//routing
$result = null;
$api = str_replace(".", "", $get[0]);
$api = "api/" . $sprint[$api] . '/' . $api . ".php";
if (file_exists($api)) {
    require_once $api;
}

//check result
if (!is_array($result)) {
    $error = "parameter is incorrect";
    if (!$is_token) {
        $error = "authorize";
    }

    $result = array(
        "messageCode" => 20000,
        "messageDescription" => $error,
        "messageStatus" => "fail",
    );
}

//switch language
$key = 'messageDescription';
$key2 = 'language';
if (is_array($result[$key])) {
    $index = $post[$key2] === 'TH' ? 0 : 1;
    $result[$key] = $result[$key][$index];
}

//ob_clean();
header('Content-Type: application/json; charset=utf-8');
echo json_encode($result);

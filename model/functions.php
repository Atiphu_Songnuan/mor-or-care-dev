<?php
/*  =============================================
Name : Pratomrerk
Email : Pratomrerk@gmail.com
============================================= */
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

class class_functions
{
    public function is_body($body, $keys)
    {
        $res = false;
        $keys_in_body = array();

        if (count($body) != 0) {
            if (count($body) == count($keys)) {
                foreach ($body as $k => $v) {
                    array_push($keys_in_body, $k);
                }

                //Check duplicated key
                //TRUE key is duplicated
                //FALSE key is unduplicated
                if ($keys_in_body === $keys) {
                    foreach ($body as $v) {
                        if (isset($v)) {
                            $res = true;
                        }
                    }
                }
            }
        }
        return $res;
    }

    public function is_input($input, $keys){
        foreach ($keys as $v) {
            if (!isset($input[$v]))  return false;
        }
        return true;
    }

    public function AccessToken($connect, $grantType, $clientId, $hospitalNumber)
    {
        global $c_sry, $jwt;
        $response = array();

        if (strlen($grantType) == 0) {
            $response["error"] = "Please, Enter your grant type.";
            return $response;
        }

        if (strlen($clientId) == 0) {
            $response["error"] = "Please, Enter your client id.";
            return $response;
        }

        if (strlen($hospitalNumber) == 0) {
            $response["error"] = "Please, Enter your hospital number.";
            return $response;
        }

        // $config = array(
        //         'host'=>'localhost:3333',
        //         'user'=>'root',
        //         'passwd'=>'',
        //         'dbname'=>'scbtest'
        // );

        $login = new class_mysql();
        $login->connectSQL($connect);

        $sql = "SELECT HOSPITAL_NUMBER
				FROM patients
				WHERE HOSPITAL_NUMBER = '$hospitalNumber'
				LIMIT 1";
        $data = $login->run_sql_return_array1d($sql);

        if (isset($data['HOSPITAL_NUMBER'])) {
            $user = array(
                'hospitalNumber' => intval($data['HOSPITAL_NUMBER']),
            );

            $token = $c_sry->jwt->encode(json_encode($user));
            $response = array(
                "messageCode" => 10000,
                "messageDescription" => "access token success",
                "messageStatus" => "success",
                "accessToken" => $token,
                "tokenType" => "Bearer",
                "expiresIn" => 86400,
            );
        } else {
            $response = array(
                "messageCode" => 400,
                "messageDescription" => "access token failed",
                "messageStatus" => "fail",
            );
        }

        return $response;
    }

    public $date_thai = array();
    public $date_thai_full = array();
    private $date_tmp = array();
    private $date_tmp_full = array();
    public function cov_date($time_)
    {
        if (count($this->date_tmp) == 0) {
            $this->date_tmp = explode(" ", " ม.ค. ก.พ. มี.ค. เม.ย. พ.ค. มิ.ย. ก.ค. ส.ค. ก.ย. ต.ค. พ.ย. ธ.ค.");
            $this->date_tmp_full = explode(" ", " มกราคม กุมภาพันธ์ มีนาคม เมษายน พฤษภาคม มิถุนายน กรกฎาคม สิงหาคม กันยายน ตุลาคม พฤศจิกายน ธันวาคม");
            for ($i = 0; $i < count($this->date_tmp); $i++) {
                $ay = $i > 9 ? $i : "0" . $i;
                $this->date_thai[$ay] = $this->date_tmp[$i];
                $this->date_thai_full[$ay] = $this->date_tmp_full[$i];
            }
        }
        $time = date('j,m,Y#H,i,s', $time_);
        $data = explode("#", $time);
        $date = explode(",", $data[0]);
        $time = explode(",", $data[1]);
        $return_time["hour"] = $time[0];
        $return_time["minute"] = $time[1];
        $return_time["second"] = $time[2];
        $return_time["day"] = $date[0];
        $return_time["month_th_mini"] = $this->date_thai[$date[1]];
        $return_time["year_th"] = $date[2] + 543;
        $return_time["year_th_mini"] = substr($return_time["year_th"], 2);
        $return_time["year"] = $date[2];
        $return_time["year_mini"] = substr($return_time["year"], 2);
        $return_time["month"] = $date[1];
        $return_time["month_th"] = $this->date_thai_full[$date[1]];
        return $return_time;
    }
    public function format_datetime($time)
    {
        $time = $this->cov_date($time);
        return $time["day"] . " " . $time["month_th_mini"] . " " . $time["year_th_mini"] . ", " . $time["hour"] . ":" . $time["minute"] . " น.";
    }
    public function format_date($time, $year_full = true)
    {
        $time = $this->cov_date($time);
        return $time["day"] . " " . $time["month_th_mini"] . " " . ($year_full ? $time["year_th"] : $time["year_th_mini"]);
    }
    public function format_time($time, $addl = true)
    {
        $time = $this->cov_date($time);
        return $time["hour"] . ":" . $time["minute"] . ($addl ? " น." : "");
    }
    public function count_datetime($date, $setting = array('cut_time' => false, 'limit_time' => 'day', 'show' => 2, 'showL' => true))
    { //นับเวลา อดีต

        $setting = array(
            'cut_time' => isset($setting['cut_time']) ? $setting['cut_time'] : false,
            'limit_time' => isset($setting['limit_time']) ? $setting['limit_time'] : 'day',
            'show' => isset($setting['show']) ? $setting['show'] : 2,
            'showL' => isset($setting['showL']) ? $setting['showL'] : ture,
        );

        $txt = "";
        $days = time() - $date;
        $list = array(
            'year' => array("ปี", 2592000 * 12),
            'month' => array("เดือน", 86400 * 30),
            'day' => array("วัน", 86400),
            'hour' => array("ชั่วโมง", 3600),
            'munite' => array("นาที", 60),
            'sec' => array("วินาที", 1),
        );

        // เกิน limit_time ให้ แสดงเวลาเต๊ม
        if ($setting['limit_time'] != '') {
            $today = $list[$setting['limit_time']];
            if (floor($days / $today[1]) > 0) {
                if ($setting['cut_time']) {
                    return $this->format_date($date);
                } else {
                    return $this->format_datetime($date);
                }
            }
        }

        $li = 0;
        foreach ($list as $value) {
            $result = floor($days / $value[1]);
            if ($result > 0 && $li < $setting['show']) { //แสดงแค่
                $txt .= $result . ' ' . $value[0] . ' ';
                $li++;
            }
            $days -= $value[1] * $result;
        }
        return ($txt == "") ? "สักครู่" : $txt . ($setting['showL'] ? "ที่แล้ว" : "");
    }

    public function check_hospital_number_length($hospitalNumber)
    {
        $hnCount = strlen($hospitalNumber);
        if ($hnCount <= 7) {
            if ($hnCount < 7) {
                for ($i = 0; $i < (7 - $hnCount); $i++) {
                    $hospitalNumber = "0" . $hospitalNumber;
                }
            }
        } else {
            $hospitalNumber = null;
        }
        return $hospitalNumber;
    }

    public function echoDatetime($datetime){
        return str_replace(array('-', ':', ' '), '', $datetime);
    }

}

<?php
/*  =============================================
Name : Pratomrerk
Email : Pratomrerk@gmail.com
v.1.0 12/09/19
============================================= */
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

class accessToken
{

    private $con;
    public function __construct($con)
    {
        $this->con = $con;
    }

    public function get()
    {
        $sql = "SELECT access_token, expires_in FROM HOSAPP.AccessToken LIMIT 1";
        $data = $this->con->run_sql_return_array1d($sql);
        $expires_in = isset($data['expires_in']) ? strtotime($data['expires_in']) : 0;

        if ($expires_in <= time()) {
            $token = $this->refresh();
            if($token['status']){
                $expires_in = strtotime($token['expiresDateTime']);
                $expires_in = date("Y-m-d H:i:s", $expires_in);
                $sql = "UPDATE HOSAPP.AccessToken SET access_token = '$token[accessToken]', expires_in = '$expires_in' LIMIT 1";
                $this->con->run_sql($sql);
                return array(
                    'accessToken' => $token['accessToken']
                );
            }else{
                $this->saveLog("[error] status: false");
                return array(
                    'accessToken' => null
                );
            }
        } else {
            return array(
                'accessToken' => $data['access_token'],
            );
        }

    }

    public function refresh()
    {
        global $CONFIGS;
        $url = $CONFIGS['api'] . 'psuGetAccessToken';

        $access_user = $CONFIGS['access_user'];
        $data = array(
            'grantType' => 'client_credentials',
            'clientId' => $access_user['clientId'],
            'username' => $access_user['username'],
        );
        $header = array(
            'User-Agent:Medicine PSU - Access Token',
            'Content-Type:application/x-www-form-urlencoded'
        );

        $context = stream_context_create(
            array(
                'http' => array(
                    'method' => 'POST',
                    'header' => implode("\r\n", $header),
                    'content' => http_build_query($data),
                ),
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ),
            )
        );

        $response = file_get_contents($url, false, $context);
        $response = json_decode($response, true);

        $this->saveLog("[refresh] status: $response[status], expiresDateTime: $response[expiresDateTime]");
        return $response;
    }

    private function saveLog($txt){
        $file = ROOTPATH."/logs/token/token-".date("Y-m-d").".txt";
        $txt = date("h:i:s")."\t".$txt."\n";
        @file_put_contents($file, $txt, FILE_APPEND);
    }

}

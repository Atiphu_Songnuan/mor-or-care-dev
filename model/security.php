<?php
/*  =============================================
Name : Pratomrerk
Email : Pratomrerk@gmail.com
v.3.3 13/03/2019
============================================= */
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

class class_security
{
    public $rewrite = true;
    public $https = true;
    public $jwt;

    public function GET_VALUE($phpname)
    {
        global $c_sry;
        $URL = $c_sry->geturl();
        $arr = array();
        if ($this->rewrite) {
            $file = pathinfo(realpath(dirname($phpname)));
            $file = strtolower($file['basename']);
            $dirRoot = "$_SERVER[HTTP_HOST]/$file/";
            $req = str_replace($dirRoot, "", $URL);
        } else {
            $req = explode('?/', $URL);
            $req = str_replace($req[0] . '?/', '', $URL);
        }
        $req = explode('/', $req);
        $count_req = count($req);
        for ($i = 0; $i < $count_req; $i++) {
            $arr[] = $this->cutSQLi($req[$i]);
        }
        if ($this->rewrite) {
            if ($arr[0] == $_SERVER['HTTP_HOST']) {
                array_shift($arr);
            }
        } else {
            if ($arr[0] == $_SERVER['HTTP_HOST']) {
                $arr = array();
            }
        }
        if ($arr[0] == $_SERVER['HTTP_HOST']) {
            $arr = array();
        }
        return $arr;
    }

    public function POST_VALUE()
    {
        $requestData = @file_get_contents('php://input');
        if ($requestData != null || $requestData != "") {
            $remark = "SCB calling HOSPITAL service";
            $newRequestData = date("Y-m-d H:i:s") . "\t". $this->getip() . "\tservice: " . $_SERVER['REQUEST_URI'] . "\tdata: " . trim(preg_replace('/\s/', '', $requestData)) . PHP_EOL;
            $lines = "";
            for ($i = 0; $i < 150; $i++) {
                $lines .= "=";
            }
            @file_put_contents(__DIR__ . "/../logs/recieved/" . "recieved-".date("Y-m-d") . ".txt", $newRequestData . $lines . PHP_EOL, FILE_APPEND);
        }
        return json_decode(@file_get_contents('php://input'), true);
    }

    public function getip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $this->cutSQLi($ipaddress);
    }

    public function getuagent()
    {
        return $this->cutSQLi($_SERVER["HTTP_USER_AGENT"]);
    }

    public function protocol()
    {
        return $this->https ? 'https://' : 'http://';
    }

    public function indexurl()
    {
        return $this->protocol() . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    }

    public function hosturl()
    {
        return $this->protocol() . $_SERVER['HTTP_HOST'] . $this->getpath();
    }

    public function getpath()
    {
        return str_replace("/index.php", "", $_SERVER['PHP_SELF']);
    }

    public function geturl()
    {
        return $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    public function getdatetime()
    {
        return date("Y-m-d H:i:s");
    }

    public function getUserInfo()
    {
        global $_POST;
        $txt = "";
        foreach ($_POST as $key => $value) {
            $txt .= "$key = $value; ";
        }
        return array(
            'data_time' => $this->getdatetime(),
            'ip' => $this->getip(),
            'session' => session_id(),
            'agent' => $this->getuagent(),
            'url' => $this->geturl(),
            'post' => $txt,
        );
    }

    public function show_allInfo($req = true)
    {
        $info = $this->getUserInfo();
        $br = "<br />";
        $txt = "Data Time: " . $info['data_time'];
        $txt .= $br . "IP: " . $info['ip'];
        $txt .= $br . "Agent: " . $info['agent'];
        $txt .= $br . "Session: " . $info['session'];
        if ($req) {
            $txt .= $br . "URL: " . $info['url'];
            $txt .= $br . "POST: " . $info['post'];
        }
        return $txt;
    }

    public function is_mobile()
    {
        $regex_match = "/(nokia|iphone|android|motorola|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|";
        $regex_match .= "htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|";
        $regex_match .= "blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam\-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|";
        $regex_match .= "symbian|smartphone|midp|wap|phone|iphone|windows ce|iemobile|^spice|^bird|^zte\-|longcos|pantech|gionee|^sie\-|portalmmm|";
        $regex_match .= "jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220";
        $regex_match .= ")/i";
        return isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']) or preg_match($regex_match, strtolower($_SERVER['HTTP_USER_AGENT']));
    }

    public function is_ie()
    {
        //ie 1-8 return true
        return preg_match('/(?i)msie [1-8]/', $_SERVER['HTTP_USER_AGENT']);
    }

    //เตือนสติ!! Function นี้ป้องกันได้เบื่องต้น
    //วิธีที่ดีที่สุดคือเขียนเสร็จแล้วต้องลองเจาะด้วยตัวเอง
    public function cutSQLi($txt)
    {
        $txt = urldecode(trim($txt));
        $search = array("'", '"', "\\");
        $replace = array("&#39;", "&quot;", "\\\\");
        $txt = str_replace($search, $replace, $txt); //ป้องกัน SQL Injection เบื้องต้น
        $txt = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $txt); //ป้องกัน XSS เบื้องต้น
        //$txt = preg_replace('{\\\\}',"\\",$txt);
        //$txt = htmlentities($txt);
        //mysql_real_escape_string
        return $txt;
    }
    public function recutSQLi($txt)
    {
        $search = array("'", '"', "\\");
        $replace = array("&#39;", "&quot;", "\\\\");
        $txt = str_replace($replace, $search, $txt);
        //$txt = html_entity_decode($txt);
        return $txt;
    }

    public function isValidEmail($email)
    {
        return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email);
    }

    public function isPhoneNo($no)
    {
        return preg_match("/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3,4})$/", $no);
    }

    public function check_id_card($id)
    {
        if (strlen($id) != 13) {return false;}
        for ($i = 0, $sum = 0; $i < 12; $i++) {
            $sum += (int) ($id{$i}) * (13 - $i);
        }
        return (11 - ($sum % 11)) % 10 == (int) ($id{12});
    }

    public function numThaitoArob($txt)
    {
        $valid = '๐ ๑ ๒ ๓ ๔ ๕ ๖ ๗ ๘ ๙';
        $keyword = explode(' ', $valid);
        for ($i = 0; $i < count($keyword); $i++) {
            $txt = str_replace($keyword[$i], $i, $txt);
        }
        return $txt;
    }

    public function IsThai($txt)
    {
        $valid = 'ก ข ฅ ค ฅ ฆ ง จ ฉ ช ซ ฌ ญ ฎ ฏ ฐ ฑ ฒ ณ ด ต ถ ท ธ น บ ป ผ ฝ พ ฟ ภ ม ย ร ล ว ศ ษ ส ห ฬ อ ฮ ๑ ๒ ๓ ๔ ๕ ๖ ๗ ๘ ๙ ๐ ะ า อ ิ อ ี อ ุ อ ู เ อ ื อ อ ึ อ ื อ ไ ๆ โ ใ ฯ';
        $keyword = explode(' ', $valid);
        return $this->IsStrArr($txt, $keyword);
    }

    public function IsEng($txt)
    {
        $valid = 'a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z';
        $keyword = explode(' ', $valid);
        return $this->IsStrArr($txt, $keyword);
    }

    public function IsStrArr($txt, $array)
    {
        foreach ($keyword as $each_word) {
            if (eregi($each_word, $txt)) {
                return true;
            }
        }
        return false;
    }

    public function randStr($chars = '', $num = '10')
    {
        $string = "";
        $chars = strlen($chars) == 0 ? '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz' : $chars;
        for ($i = 0; $i < $num; $i++) {
            $pos = rand(0, strlen($chars) - 1);
            $string .= $chars{$pos};
        }
        return $string;
    }

    public function set_master_key_encryption()
    {
        global $c_sql;
        $sql = "SELECT (SELECT value FROM `control_tb` WHERE config='logfile') AS logfile,
					   (SELECT value FROM `control_tb` WHERE config='master_key_encryption') AS master_key_encryption";
        $data = $c_sql->run_sql_return_array1d($sql, false);
        $this->logfile = $data['logfile'];
        $this->master_key_encryption = $data['master_key_encryption'];
        $this->jwt = new jwt();
        $this->jwt->secret = $this->master_key_encryption;
        return $this->master_key_encryption;
    }

    public function get_token($type = "Bearer")
    {
        $user = null;
        $headers = getallheaders();
        if (isset($headers['Authorization'])) {
            $auth = explode(" ", $headers['Authorization']);
            if ($auth[0] == $type) {
                $user = json_decode($this->jwt->decode($auth[1]), true);
            }
        }
        return $user;
    }

    public function set_token($user, $expire = 86400)
    {
        setcookie('access_token', $this->jwt->encode(json_encode($user)), time() + $expire, "/");
    }

    public function del_token()
    {
        if (isset($_COOKIE['access_token'])) {
            unset($_COOKIE['access_token']);
            setcookie('access_token', null, -1, '/');
        }
    }

    public function get_authorization($type = "Basic")
    {
        $headers = getallheaders();
        if (!isset($headers['Authorization'])) {
            $this->header401();
        }

        return str_replace("$type ", "", $headers['Authorization']);
    }

    //====== Encryption =========//

    //Highly Secure Data Encryption & Decryption Made Easy with PHP, MCrypt, Rijndael-256, and CBC
    //http://www.warpconduit.net/2013/04/14/highly-secure-data-encryption-decryption-made-easy-with-php-mcrypt-rijndael-256-and-cbc/

    public $logfile = "";
    private $master_key_encryption = "";

    public function de_code($decrypt)
    {
        $key = bin2hex($this->master_key_encryption); //edition
        $decrypt = explode('|', $decrypt . '|');
        $decoded = base64_decode($decrypt[0]);
        $iv = base64_decode($decrypt[1]);
        if (strlen($iv) !== mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)) {return false;}
        $key = pack('H*', $key);
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
        $mac = substr($decrypted, -64);
        $decrypted = substr($decrypted, 0, -64);
        $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
        if ($calcmac !== $mac) {return false;}
        $decrypted = unserialize($decrypted);
        return $decrypted;
    }

    public function en_code($encrypt)
    {
        $key = bin2hex($this->master_key_encryption); //edition
        $encrypt = serialize($encrypt);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
        $key = pack('H*', $key);
        $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt . $mac, MCRYPT_MODE_CBC, $iv);
        $encoded = base64_encode($passcrypt) . '|' . base64_encode($iv);
        return $encoded;
    }

    //One Way Encryption

    public function en_oneWay($txt)
    {
        $key = bin2hex($this->master_key_encryption); //edition
        $txt = strtoupper(hash_hmac('sha256', $txt, substr(bin2hex(pack('H*', $key)), -32)));
        return $txt;
    }

}

class jwt
{

    public $secret = "";
    public $alg = "HS256";

    private static $supported_algs = array(
        'HS256' => array('hash_hmac', 'SHA256'),
        'HS512' => array('hash_hmac', 'SHA512'),
        'HS384' => array('hash_hmac', 'SHA384'),
        'RS256' => array('openssl', 'SHA256'),
        'RS384' => array('openssl', 'SHA384'),
        'RS512' => array('openssl', 'SHA512'),
    );

    public function encode($data)
    {

        $header = json_encode([
            'typ' => 'JWT',
            'alg' => $this->alg,
        ]);

        $header = $this->base64Url_encode($header);
        $payload = $this->base64Url_encode($data);

        $signing = $header . "." . $payload;
        $signature = $this->sign($signing);
        if (strlen($signature) == 0) {
            return "";
        }

        $signature = $this->base64Url_encode($signature);
        $jwt = $header . "." . $payload . "." . $signature;
        return $jwt;
    }

    public function decode($jwt)
    {

        $tks = explode('.', $jwt);
        if (count($tks) != 3) {
            return "";
        }

        list($header, $payload, $signature) = $tks;
        $signature = $this->base64Url_decode($signature);
        $alg = json_decode($this->base64Url_decode($header))->alg;
        if ($alg != $this->alg) {
            return "";
        }

        if (!$this->verify("$header.$payload", $signature)) {
            return "";
        }

        return $this->base64Url_decode($payload);
    }

    private function base64Url_encode($value)
    {
        return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($value));
    }

    private function base64Url_decode($value)
    {
        return base64_decode(str_replace(['-', '_', ''], ['+', '/', '='], $value));
    }

    private function sign($msg)
    {

        list($func, $alg) = static::$supported_algs[$this->alg];

        switch ($func) {
            case 'hash_hmac':
                return hash_hmac($alg, $msg, $this->secret, true);
            case 'openssl':
                $signature = '';
                $success = openssl_sign($msg, $signature, $this->secret, $alg);
                if (!$success) {
                    return "";
                } else {
                    return $signature;
                }
        }
    }

    private function verify($msg, $signature)
    {

        list($func, $alg) = static::$supported_algs[$this->alg];

        switch ($func) {
            case 'openssl':
                $success = openssl_verify($msg, $signature, $this->secret, $alg);
                if ($success === 1) {
                    return true;
                }
                return false;
            case 'hash_hmac':
            default:
                $hash = hash_hmac($alg, $msg, $this->secret, true);
                if (function_exists('hash_equals')) {
                    return hash_equals($signature, $hash);
                }
                $len = min($this->safeStrlen($signature), $this->safeStrlen($hash));
                $status = 0;
                for ($i = 0; $i < $len; $i++) {
                    $status |= (ord($signature[$i]) ^ ord($hash[$i]));
                }
                $status |= ($this->safeStrlen($signature) ^ $this->safeStrlen($hash));
                return ($status === 0);
        }
    }

    private function safeStrlen($str)
    {
        if (function_exists('mb_strlen')) {
            return mb_strlen($str, '8bit');
        }
        return strlen($str);
    }

}

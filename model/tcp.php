<?php
    header('Content-Type: application/json; charset=utf-8');
    set_time_limit(30);

    //get data
    $header = getallheaders();
    $body = file_get_contents('php://input');
    $body = str_replace("\r\n", "", $body);

    //setting server
    $server = array(
        'ip' => '127.0.0.1',
        'port' => 7519
    );

    $ticket = uniqid(rand(), true);

    //pack data
    $payload = array(
        'ticket'    => $ticket,
        'url'       => $_SERVER['REQUEST_URI'],
        'header'    => $header,
        'body'      => [$body],
    );
    $payload = json_encode($payload);
    $payload = str_replace(array("[\"{", "}\"]","\\\""), array("{","}","\""), $payload);
    $payload = "02".chr(0x1).$payload;

    //connect server
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {
        echo "{\"error\":\"".socket_strerror(socket_last_error())."\"}";
    }

    if(connect_server()){

        //sending data to server
        socket_write($socket, $payload, strlen($payload));
        //reading response
        while ($out = socket_read($socket, 2048)) {
            echo $out;
        }
        //closing socket
        socket_close($socket);

    }else{
        echo "{\"error\":\"connect server failed\"}";
    }




    function connect_server(){
        global $socket, $server;

        $reconnect_count = 0;
        $reconnect_max = 3;
        $connect = true;
    
        do {
            $reconnect_count++;
            $result = socket_connect($socket, $server['ip'], $server['port']);
            if($result === false){
                $connect = false;
            }
            if($reconnect_count >= $reconnect_max){
                break;
            }
        } while ($connect == false);

        return $connect;
    }

?>
<?php
/*  =============================================
      	Name : Pratomrerk
		Email : Pratomrerk@gmail.com
		v.1.2 16/01/2019  
    ============================================= */
	if(!defined('SECURITY')){header("HTTP/1.1 404 Not Found");die;}

class class_files{
	
	public function file_get($path){
		$text = @file_get_contents($path);
		//remove the utf-8 BOM
		$bom = pack('H*','EFBBBF');
    	$text = preg_replace("/^$bom/", '', $text);
		return $text;
	}
	public function file_put($path, $str, $append = false){
		$res = false;
		if($append){
			$res = @file_put_contents($path, $str, FILE_APPEND);
		}else{
			$res = @file_put_contents($path, $str);
		}
		return $res;
	}

	public function upzip($zip_path, $path, $select_file = array()){
		$zip = new ZipArchive;
		$res = $zip->open($zip_path);
		if ($res === true) {
		    if(count($select_file)>0){
		    	$zip->extractTo($path, $select_file);
		    }else{
		    	$zip->extractTo($path);
			}
		    $zip->close();
		    return true;
		} else {
		    return false;
		}
	}

	public function view_type($file_name){
		return (substr(strrchr($file_name, "."), 1));
	}
	
	public function ReSizeIMG($images, $width){
		$size=GetimageSize($images);
		if($size[0]>$width){
			$height=round($width*$size[1]/$size[0]);
			$images_orig = ImageCreateFromJPEG($images);
			$photoX = ImagesX($images_orig);
			$photoY = ImagesY($images_orig);
			$images_fin = ImageCreateTrueColor($width, $height);
			ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
			ImageJPEG($images_fin,$images);
			ImageDestroy($images_orig);
			ImageDestroy($images_fin);
		}
	}
	
	public function viewFileDir($dir, $view_permis=true, $view_date=true, $view_size=true){
		$dirlist = scandir($dir);
		$filelist = array();
		foreach ($dirlist as $value) {
			if($value!='.' && $value!='..'){
				$filelist[] = array(
									'permission'=> (($view_permis)? $this->get_permissions($value):''),
									'file'=> $value,
									'date'=> (($view_date)? date("F d Y H:i:s.", filemtime($filename)):''),
									'size'=> (($view_size)? number_format(@filesize($value) / 1048576, 2):'')
								);
			}
		}
		return $filelist;
	}
	
	public function delDir($dir){
		$msg = array();
		$filein = $this->viewFileDir($dir, false);
		foreach ($filein as $value) {
			$file = $dir.'/'.$value['file'];
			$msg[$file] = (@unlink($file)?true:false);
		}
		return array(
			'dir' => (@rmdir($dir)?true:false),
			'files' => $msg
		);
	}

	public function get_permissions($addr){
		$perms = fileperms($addr);
		
		if (($perms & 0xC000) == 0xC000) {
			// Socket
			$info = 's';
		} elseif (($perms & 0xA000) == 0xA000) {
			// Symbolic Link
			$info = 'l';
		} elseif (($perms & 0x8000) == 0x8000) {
			// Regular
			$info = '-';
		} elseif (($perms & 0x6000) == 0x6000) {
			// Block special
			$info = 'b';
		} elseif (($perms & 0x4000) == 0x4000) {
			// Directory
			$info = 'd';
		} elseif (($perms & 0x2000) == 0x2000) {
			// Character special
			$info = 'c';
		} elseif (($perms & 0x1000) == 0x1000) {
			// FIFO pipe
			$info = 'p';
		} else {
			// Unknown
			$info = 'u';
		}
		
		// Owner
		$info .= (($perms & 0x0100) ? 'r' : '-');
		$info .= (($perms & 0x0080) ? 'w' : '-');
		$info .= (($perms & 0x0040) ?
					(($perms & 0x0800) ? 's' : 'x' ) :
					(($perms & 0x0800) ? 'S' : '-'));
		
		// Group
		$info .= (($perms & 0x0020) ? 'r' : '-');
		$info .= (($perms & 0x0010) ? 'w' : '-');
		$info .= (($perms & 0x0008) ?
					(($perms & 0x0400) ? 's' : 'x' ) :
					(($perms & 0x0400) ? 'S' : '-'));
		
		// World
		$info .= (($perms & 0x0004) ? 'r' : '-');
		$info .= (($perms & 0x0002) ? 'w' : '-');
		$info .= (($perms & 0x0001) ?
					(($perms & 0x0200) ? 't' : 'x' ) :
					(($perms & 0x0200) ? 'T' : '-'));

		return $info;
	}
	
	public function help_perms($chars){
		$help = array(
			array(
					'-' => 'File', 
					'd' => 'Directory',
					'l' => 'Symlink', 
					's' => 'Socket', 
					'p' => 'Pipe', 
					'c' => 'Character (unbuffered) device', 
					'b' => 'Block (buffered) device'
			),
			array(
					'-' => 0,
					'x' => 1,
					'w' => 2,
					'r' => 4
			),
			array(
					'-' => '',
					'x' => 'Execute',
					'w' => 'Write',
					'r' => 'Read'
			)
		);
		$type = $help[0][$chars{0}];
		
		$h = $help[1];
		$user = $h[$chars{1}]+$h[$chars{2}]+$h[$chars{3}];
		$group = $h[$chars{4}]+$h[$chars{5}]+$h[$chars{6}];
		$other = $h[$chars{7}]+$h[$chars{8}]+$h[$chars{9}];
		
		$h = $help[2];
		$user_perms = $h[$chars{1}].', '.$h[$chars{2}].', '.$h[$chars{3}];
		$group_perms = $h[$chars{4}].', '.$h[$chars{5}].', '.$h[$chars{6}];
		$other_perms = $h[$chars{7}].', '.$h[$chars{8}].', '.$h[$chars{9}];
		
		return array(
			'type' => $type,
			'number' => "$user$group$other",
			'perms'=>"User:$user_perms;Group:$group_perms;Other:$other_perms;"
		);
	}
	
}
?>
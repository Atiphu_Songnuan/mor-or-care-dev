<?php
/*  =============================================
      	Name : Pratomrerk
		Email : Pratomrerk@gmail.com
		v.1.0 (build 100919)
		Ref: https://medium.com/@phayao/แนวออกแบบ-restful-api-วิธีปฏิบัตที่ดี-c320d806e30b
    ============================================= */
	if(!defined('SECURITY')){header("HTTP/1.1 404 Not Found");die;}

class restful{

	private $query = "";

	public function toSqlQuery($paras = array()){

		$this->query = isset($paras['query'])? $paras['query'] : $_SERVER['QUERY_STRING'];
		$index = isset($paras['index'])? $paras['index'] : 0;
		$method = isset($paras['method'])? $paras['method'] : $this->getMethod();
		$table = isset($paras['table'])? $paras['table'] : "";
		$database = isset($paras['database'])? $paras['database']."." : "";
		$field = isset($paras['field'])? $paras['field'] : "*";
		$data = isset($paras['data'])? $paras['data'] : json_decode(@file_get_contents('php://input'), true);
		$limit = isset($paras['limit'])? intval($paras['limit']) : 30;
		$limit_update = isset($paras['limit_update'])? $paras['limit_update'] : 1;
		$limit_delete = isset($paras['limit_delete'])? $paras['limit_delete'] : 1; 
		$shift = isset($paras['shift'])? $paras['shift']: 1;
		$key = isset($paras['key'])? $paras['key']: "";
		$access_method = isset($paras['access_method'])? $paras['access_method']: $this->method_used;

		if(empty($table)){
			$point = $this->getEndpoint($index, $shift);
			$table = $point['key'];
			$root_value = $point['value'];
		}
		if(empty($table)) $this->return_status(400, true);
		if(!in_array($method, $access_method)) $this->return_status(403, true);

		switch ($method) {
			
			case 'PUT':
				$action = isset($paras['replace'])? 'REPLACE' : 'INSERT';
				$v = $this->getValues($data);
				$columns = $v['field'];
				$values = $v['values'];
				if(empty($values)) $this->return_status(400, true);
				$sql = "$action INTO $database$table\n($columns) VALUES ($values)";
				break;

			case 'POST':
			case 'DELETE':
			default:
		
				//Columns
				if(is_array($field)){
					$columns = $this->getValues($field, false)['values'];
				}else{
					$columns = $field;
				}
				
				//Where
				$where = "";
				$i = 0;
				if(!empty($root_value)){
					if(!empty($key)){
						$value = $this->strValue($root_value);
						$where = $key." = $value && ";
						$index++;
					}else
					if(is_array($field)){
						$value = $this->strValue($root_value);
						$where = $field[0]." = $value && ";
						$index++;
					}
				}
				$resource = $this->getEndpoint($index, $shift);
				while(!empty($resource['key']) && !empty($resource['value'])){
					$value = $this->strValue($resource['value']);
					$where .= $resource['key']." = $value  && ";
					$i++;
					$resource = $this->getEndpoint($index + $i, $shift);
				}
				if(strlen($where)>= 3) $where = substr($where, 0, -3);
				if(empty($where)) $where = "1";

				//Or
				$like = $this->getFiltering();
				if(!empty($like)){
					$slike = "";
					foreach ($like as $k => $v) {
						$v = $this->strValue($v);
						$slike.= "$k = $v || ";
					}
					$where = $where == "1"? "":"$where && ";
					$slike = trim(substr($slike, 0, -3));
					$where .= "($slike)";
				}

				//Like
				if($columns != '.'){
					$search = $this->getSearching();
					if(!empty($search)){
						$where = $where == "1"? "":"$where && ";
						$search = $this->strValue($search);
						$where.= "MATCH($columns) AGAINST ($search) IN BOOLEAN MODE ";
					}
				}

				//Order
				$order = $this->getSorting();
				if(!empty($order)){
					$order_by = 'ORDER BY ';
					foreach ($order as $v) {
						$order_by.= "$v[0] $v[1], ";
					}
					$order_by = trim(substr($order_by, 0, -2));
					$order_by.= "\n";
				}

				//Limit
				$page = $this->getPagination();
				$page = $page * $limit;


				switch ($method) {
 
					case 'POST':
						$set = '';
						foreach ($data as $k => $v) {
							$v = $this->strValue($v);
							$set.= "$k = $v, ";
						}
						if(empty($set)) $this->return_status(400, true);
						$set = trim(substr($set, 0, -2));
						$sql = "UPDATE $database$table\nSET $set\nWHERE $where\nLIMIT $limit_update"; 
						break;

					case 'DELETE':
						$sql = "DELETE FROM $database$table\nWHERE $where\nLIMIT $limit_delete";
						break;
					
					default: //GET
						$sql = "SELECT $columns\nFROM $database$table\nWHERE $where\n".$order_by."LIMIT";
						$sql.= ($page == 0 || $limit == 1)? " $limit":" $page,$limit";
						break;
				}

				break;
		}

		return "$sql;";
	}

	private $method_used = array('GET','POST', 'PUT', 'DELETE');

	public function getMethod(){
		$method = $_SERVER['REQUEST_METHOD'] == 'POST'? 'POST':'GET';
		$method = isset($_SERVER['HTTP_X_REST_METHOD'])? strtoupper($_SERVER['HTTP_X_REST_METHOD']):$method;
		foreach ($this->method_used as $v) {
			if($method == $v){
				return $v;
			}
		}
		return $this->method_used[0];
	}

	public function isGet(){
		return $restful->getMethod() == 'GET'? true:false;
	}
	
	public function getEndpoint($index, $shift = 1){
		$query = strlen($this->query) == 0? $_SERVER['QUERY_STRING']:$this->query;
		$arg = parse_url($query);
		$req = explode('/', $arg['path']);
		for ($i=0; $i < $shift; $i++) { 
			array_shift($req);
		}
		$index = $index * 2;
		$key = isset($req[$index])? $req[$index]:"";
		$value = isset($req[$index+1])? $req[$index+1]:"";
		return array(
			'key' => $key,
			'value' => $value,
		);
	}

	private function getParams(){
		$query = strlen($this->query) == 0? $_SERVER['QUERY_STRING']:$this->query;
		$arg = parse_url($query);
		$data = array();
		parse_str($arg['query'], $paras);
		foreach ($paras as $key => $value) {
			$data[$key] = trim($value);
		}
		return $data;
	}

	private function getValues($arr, $strValue = true){
		$field = array();
		$values = array();
		foreach ($arr as $key => $value) {
			$field[] = $key;
			$values[] = $strValue? $this->strValue($value) : $value;
		}
		return array(
			'field' => join(',', $field),
			'values' => join(',', $values)
		);
	}

	private function strValue($value){
		$value = is_numeric($value) && gettype($value) != 'string'? $value : "'$value'";
		if(strlen($value) > 0) $value = (substr($value, 0, 1) == '0' && strlen($value) > 1)? "'$value'" : $value;
		return $value;
	}

	public function getSorting(){
		$get = $this->getParams();
		$value = array();
		$sort = isset($get['sort'])? explode(',', $get['sort']):"";
		foreach ($sort as $v) {
			$tmp = explode('_', $v);
			$order = strtoupper($tmp[1]) == 'ASC'? 'ASC':'DESC';
			$value[] = array(trim($tmp[0]), $order);
		}
		return $value;
	}

	public function getFiltering(){
		$get = $this->getParams();
		unset($get['sort']);
		unset($get['search']);
		unset($get['page']);
		unset($get['limit']);
		return $get;
	}

	public function getSearching(){
		$get = $this->getParams();
		return isset($get['search'])? $get['search']:"";
	}

	public function getPagination(){
		$get = $this->getParams();
		$page = isset($get['page'])? intval($get['page']):0;
		return $page < 0? 0:$page;
	}

/*
	### HTTP response status code (รหัสสถานะ) ###

	เมื่อ client สร้าง request ไปยัง server ผ่าน API, ควรจะรู้ผลสะท้อนกลับ ไม่ว่าการ request นั้น จะ Pass, Fail หรือ การ request นั้นผิด 
	โดยที่ HTTP status จะเป็นรหัสมาตรฐานที่บอกสถานะของการ request ซึ่งจะมีหลากหลายในแต่ละสถานการณ์ ซึ่ง ตัว Server เองควรจะ return status code ให้ถูกต้อง
	ต่อไปนี้จะเป็น HTTP status code ที่สำคัญโดยแบ่งออกเป็นหมวดดังนี้:

	--> 2xx (หมวด Success)
	เป็น status code ที่บอกว่าการ request นั้นได้รับแล้วและกระทำตาม method สำเร็จโดย Server
	200 Ok เป็นมาตรฐานของ HTTP response นั้น Success สำหรับ GET, PUT หรือ POST
	201 Create เป็น response สำหรับข้อมูลใหม่ได้ถูกสร้างขึ้น ใช้สำหรับ POST
	204 No Content เป็น response สำหรับ request ที่ดำเนินการ Success แต่ไม่ได้ return ข้อมูลกลับ

	DELETE เป็นตัวอย่างที่ดี, DELETE /companies/34/employees/john จะ request ให้ลบ John ใน employees และใน return เราไม่จำเป็นต้องส่งข้อมูลกลับ 
	เราร้องขอ System ให้ “ลบ” ข้อมูลออกแต่ถ้ามี error เกิดขึ้น อย่างเช่นข้อมูล John ไม่มีในระบบ แทนที่จะ return status code เป็น 2xx ในหมวด Success 
	แต่จะ return status code เป็นหมวด 4xx แทน
	
	--> 3xx (หมวด Redirection)
	304 Not Modified เป็น status code ที่บอกว่า client ได้รับการ response แล้วอยู่ใน cache และไม่จำเป็นจะต้องส่งผ่านข้อมูลเดิมอีกครั้ง

	--> 4xx (หมวด Client error)
	โดยที่ status code เหล่านี้จะบอก client ว่าสิ่งที่ request มานั้น Failed
	400 Bad Request บอกว่า request ที่ส่งมาโดย client นั้นไม่ถูกดำเนินการ และ Server ไม่เข้าใจว่า request เกี่ยวกับอะไร
	401 Unauthorized บอกว่า client ไม่ได้รับอนุญาตในการเข้าถึง Resource และควรจะ request ใหม่ด้วย credential
	403 Forbidden บ่งบอกว่า request นั้นถูกต้องและ client ได้รับการอนุญาต แต่ Client ไม่ได้รับการอนุญาตให้เข้าถึง Resource หรือหน้าเพจด้วยเหตุผลบางประการ เช่น บางครั้ง Client ที่ได้รับอนุญาต ไม่อนุญาตให้เข้าถึงระบบไฟล์
	404 Not Found บ่งบอกว่า resource ที่ request มานั้น ไม่ว่างใช้งานตอนนี้
	405 Gone บ่งบอกว่า resource ที่ต้องการนั้นไม่มีอยู่แล้ว หรืออาจจะย้ายไปที่อื่น

	--> 5xx (หมวด Server error)
	500 Internal Server Error บ่งบอกว่าการ request นั้นถูกต้อง แต่ server มีความสับสนและจะบริการด้วยเงือนไขที่คาดการไม่ได้
	503 Service Unavailable บ่งบอกว่า server ใช้การไม่ได้ หรือไม่ว่างที่จะรับและดำเนินการ request โดยส่วนใหญ่แล้ว server อยู่ในช่วงบำรุงรักษา
 */
	public function return_status($code, $exit = false){
		http_response_code($code);
		if($exit) exit;
	}


}

?>
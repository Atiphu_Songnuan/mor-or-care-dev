<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}
// header('Content-Type: charset=utf-8');
if ($c_fun->is_body($post, array("hospitalNumber", "appointmentId")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        if ($post["appointmentId"] != "") {
            $appointmentID = $post["appointmentId"];
            $appointmentDate = explode("|", $appointmentID)[1];
            $appointmentCodeAP = explode("|", $appointmentID)[2];

            $c_sql_his = new class_mysql();
            $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);
            $OPD = $isProduction? 'OPD':'DATATEST';
            $sql = "SELECT
                    SEQ AS journeyNo,
                    $OPD.Cm_ItemJourney.DETAIL AS journeyName,
                    DESCRIPTION AS journeyDetail,
                    STATUS AS journeyStatus,
                    CONCAT(REPLACE(DATE_SV, '-', ''), ' ', REPLACE(TIME_SV, ':', ''), '00') AS journeyDateTime
                    FROM
                        $OPD.Dm_HNJourney
                    INNER JOIN $OPD.Cm_ItemJourney ON Dm_HNJourney.CODE = Cm_ItemJourney.CODE
                    WHERE HN = '$hn'
                    AND C_UNIT = '$appointmentCodeAP'
                    AND DATE_AP = '$appointmentDate'
                    ORDER BY journeyNo";

            $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
            $c_sql_his->close_sql($connnect);

            $journeyStepList = array();
            if (count($data) != 0) {
                foreach ($data as $v) {
                    foreach ($v as $key => $value) {
                        if ($v[$key] == null || $v[$key] == "") {
                            $v[$key] = "";
                        } elseif ($key === "journeyStatus") {
                            if ($v[$key] === "1") {
                                $v[$key] = "complete";
                            } elseif ($v[$key] === "0") {
                                $v[$key] = "notStart";
                            }
                        }
                    }
                    array_push($journeyStepList, $v);
                }
                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "",
                    "messageStatus" => "success",
                    "journeyList" => $journeyStepList,
                );

            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "ไม่มีขั้นตอนการรักษา",
                    "messageStatus" => "fail",
                    "journeyList" => $journeyStepList,
                );
            }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "ไม่มีข้อมูลนัดหมาย",
                "messageStatus" => "fail",
            );
        }

    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}

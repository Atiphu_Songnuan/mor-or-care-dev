<?php
//WS007 Debug
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

$rfu = new restful();
$his = new class_mysql();
$connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);

$option = array(
    'user' => array(
        'shift' => 2,
        'database' => 'HOSAPP',
        'table' => 'AppUser',
        'key' => 'HN',
        'access_method' => array('GET'),
    ),
    'bill' => array(
        'shift' => 2,
        'database' => 'PAYMENT_BETA',
        'key' => 'id',
        'access_method' => array('GET','POST', 'PUT', 'DELETE'),
    ),
    'invoice' => array(
        'shift' => 2,
        'database' => 'PAYMENT_BETA',
        'key' => 'id',
        'access_method' => array('GET', 'POST', 'PUT', 'DELETE'),
    ),
    'lock' => array(
        'shift' => 2,
        'database' => 'PAYMENT_BETA',
        'key' => 'invoiceId',
        'access_method' => array('GET', 'POST', 'DELETE'),
    ),
    'payment' => array(
        'shift' => 2,
        'database' => 'PAYMENT_BETA',
        'key' => 'verifyRefID',
        'access_method' => array('GET', 'DELETE'),
    ),
    'station' => array(
        'shift' => 2,
        'database' => 'PAYMENT_BETA',
        'key' => 'id',
        'access_method' => array('GET'),
    ),
);

$enpoint = $rfu->getEndpoint(1, 0)['key'];
$op = $option[$enpoint];
if(!isset($op)) $rfu->return_status(400, true);

$sql = $rfu->toSqlQuery($op);
$result = $his->run_sql_return_array($sql, 1);
if(empty($result)) $rfu->return_status(204, true);

if($enpoint == 'payment'){
    foreach ($result as $k => $v) {
        $result[$k]['data'] = json_decode($v['jsonData'], true);
        unset($result[$k]['jsonData']);
    }
}

if(is_array($result[0]) && count($result) == 1) $result = $result[0];

$his->close_sql($connnect);

<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "language")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        $c_sql_his = new class_mysql();
        $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

        //1852261
        $Pharmacy = $isProduction? 'Pharmacy':'DATATEST';
        $sql = "SELECT  pharname AS medicine,
                        symptom AS drugAllergy,
                        if(Symp_Type='4','certain ใช่แน่นอน','probable น่าจะใช่') AS evaluationOfDrugAllergy
                FROM $Pharmacy.Drug_effect
                WHERE HN='$hn'
                AND (percancel<1
                OR isnull(percancel))
                AND Symp_Type in ('3','4')";

        $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
        $c_sql_his->close_sql($connnect);

        if (isset($data)) {
            $drugAllergyList = array();
            if (count($data) != 0) {
                //Set Phone Number list
                // $testdata = array("074541728","0958567419");
                // $allMedicalWelfares = array();
                foreach ($data as $v) {
                    foreach ($v as $key => $value) {
                        if ($v[$key] == null) {
                            $v[$key] = "";
                        }
                        
                        if ($key === "medicine") {
                            $v[$key] = trim($v[$key]);
                        }

                        if ($key === "drugAllergy") {
                            $v[$key] = trim(preg_replace('/\s\s+/', ' ', $v[$key]));
                        }
                    }
                    array_push($drugAllergyList, $v);

                }

                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "",
                    "messageStatus" => "success",
                    "drugAllergyList" => $drugAllergyList,
                );
            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "ไม่มีข้อมูลการแพ้ยา",
                    "messageStatus" => "fail",
                    "drugAllergyList" => $drugAllergyList,
                );
            }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                "messageStatus" => "fail",
            );
        }

    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}

<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "language")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        $c_sql_his = new class_mysql();
        $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);
        $Pharmacy = $isProduction? 'Pharmacy':'DATATEST';
        $Mydata = $isProduction? 'Mydata':'DATATEST';
        $OPD = $isProduction? 'OPD':'DATATEST';
        $sql = "SELECT  CONCAT(A.LANE,'/',A.DAILY_NO) AS docNo,
                        A.DISP_DATE AS orderDate,
                        A.BEG_TIME AS orderTime,
                        'แพทย์โรงพยาบาลสงขลานครินทร์' AS doctor,
                        B.U_NAME AS clinic
                FROM $Pharmacy.Disp_day as A
                LEFT JOIN $Mydata.Funit as B on A.C_OPD=B.C_UNIT
                WHERE A.HN='$hn'
                AND A.OLD_DNO<>'DDDD'
                AND A.DISP_DATE=(
                    SELECT MAX(DATE_CL)
                    FROM $OPD.Cl_regis
                    WHERE HN='$hn'
                    ORDER BY  DATE_CL DESC )
                ORDER BY A.DISP_DATE DESC";

        $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
        $c_sql_his->close_sql($connnect);

        if (isset($data)) {
            $prescriptionList = array();
            if (count($data) != 0) {
                //Set Phone Number list
                // $testdata = array("074541728","0958567419");
                // $allMedicalWelfares = array();
                foreach ($data as $v) {
                    foreach ($v as $key => $value) {
                        if ($v[$key] == null) {
                            $v[$key] = "";
                        }

                        // if ($key === "drugAllergy") {
                        //     $v[$key] = trim(preg_replace('/\s\s+/', ' ', $v[$key]));
                        // }
                    }
                    array_push($prescriptionList, $v);

                }

                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "",
                    "messageStatus" => "success",
                    "prescriptionList" => $prescriptionList,
                );
            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "ไม่มีข้อมูลการได้รับยาวันล่าสุด",
                    "messageStatus" => "fail",
                    "prescriptionList" => $prescriptionList,
                );
            }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                "messageStatus" => "fail",
            );
        }
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}

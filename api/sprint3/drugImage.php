<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "drugCode"))) {
    if ($hn === $post['hospitalNumber']) {
        // $fileList = glob('img/drugcode/' . $post['drugCode'] . '/*');
        // preg_match('~[0-9]+~', $post['drugCode'])
        // if (is_numeric("887")) { echo "Yes"; } else { echo "No"; }
        if ($post['drugCode'] != "" && $post['drugCode'] !== "0" && (strlen($post['drugCode']) >= 1 && strlen($post['drugCode']) <= 4) && (is_numeric($post['drugCode']))) {
            $fileList = glob('img/drugcode/9999/'. '*');
            // $fileList = glob('img/drugcode/'.$post['drugCode'] . '/*');

            // print_r("https://".$_SERVER['HTTP_HOST']);

            $drugList = array();
            if (count($fileList) != 0) {

                foreach ($fileList as $k => $v) {
                    // print_r($v);
                    $imageURL = "https://" . $_SERVER['HTTP_HOST'] . "/HosApp/alpha/" . $v;
                    // print_r("https://" . $_SERVER['HTTP_HOST'] . "/HosApp/alpha/" . $v);
                    $image = array(
                        "drugImage" => $imageURL,
                    );
                    array_push($drugList, $image);
                }

                // print_r($drugList);
                // $drugList = array(
                //     "drugImage" =>
                // );

                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "Get drug image success",
                    "messageStatus" => "success",
                    "drugImageList" => $drugList,
                );
            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "ไม่พบรูปภาพยา",
                    "messageStatus" => "fail",
                    "drugImageList" => $drugList,
                );
            }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "drugCode ไม่ถูกต้อง",
                "messageStatus" => "fail",
            );
        }
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}

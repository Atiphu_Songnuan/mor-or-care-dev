<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("grantType", "clientId", "hospitalNumber"))) {

    if($post['clientId'] != $CONFIGS['clientId']){
        $result = array(
            "messageCode" => 14000,
            "messageDescription" => "clientId not valid",
            "messageStatus" => "fail",
        );
    }else{
        $hn = $post['hospitalNumber'];
        $expiresIn = (60 * 60) * (24 * 30) * 6; // 6 months
        $expired = time() + $expiresIn;

        $payload = array(
            "hn" => $hn,
            "exp" => $expired
        );

        $result = array(
            "messageCode" => 10000,
            "messageDescription" => "access token success",
            "messageStatus" => "success",
            "accessToken" => $c_sry->jwt->encode(json_encode($payload)),
            "tokenType" => "Bearer",
            "expiresIn" => ($expiresIn / 60),
        );

        $is_token = true;
    }

}

<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

// print_r("IP: ". $c_sry->getip());
// $entityBody = file_get_contents('php://input');
// $rawData = json_decode( file_get_contents( 'php://input' ), true );
if ($c_fun->is_body($post, array("hospitalNumber", "typeOfID", "idNumber", "dateOfBirth", "firstName", "lastName", "fromSystem", "language"))) {

    if ($post['fromSystem'] === "HosApp") {
        $checkHN = $post['hospitalNumber'];
        $hn = $c_fun->check_hospital_number_length($checkHN);

        if (isset($hn)) {
            $c_sql_his = new class_mysql();
            $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);
            $Mydata = $isProduction ? 'Mydata' : 'DATATEST';
            $sql = "SELECT  rphone, pinitial, pname, psur
                FROM    $Mydata.Medrec
                WHERE   hn = '$hn'
                AND     pphone ='$post[idNumber]'
                AND     date(bdate) ='$post[dateOfBirth]'
                AND     pname = '$post[firstName]'
                AND     psur = '$post[lastName]'
                LIMIT   1";

            $patientData = $c_sql_his->run_sql_return_array1d($sql);
            $c_sql_his->close_sql($connnect);

            if (isset($patientData['pname'])) {
                $allPhoneNumbers = preg_replace('/[^0-9]/', '', $patientData['rphone']);

                //เอาเบอร์แรก เช่น 0995555555/0897564123 => 0995555555
                $phoneNumber = substr($allPhoneNumbers, 0, 10);
                $startMobileNumber = substr($phoneNumber, 0, 2);

                $c_sql_his = new class_mysql();
                $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

                $ename = explode(" ", GetEName($Mydata, $c_sql_his, $hn));
                $titleNameEn = "";
                $firstNameEn = "";
                $lastNameEn = "";
                if (count($ename) != 0) {
                    if (count($ename) == 3) {
                        $titleNameEn = $ename[0];
                        $firstNameEn = $ename[1];
                        $lastNameEn = $ename[2];
                    } else if (count($ename) == 2) {
                        $firstNameEn = $ename[0];
                        $lastNameEn = $ename[1];
                    } else if (count($ename) == 4) {
                        $firstNameEn = $ename[0];
                        $lastNameEn = $ename[3];
                    }
                }

                if ((strlen($phoneNumber) == 10) && ($startMobileNumber == "06" || $startMobileNumber == "08" || $startMobileNumber == "09")) {
                    $result = array(
                        "messageCode" => 10000,
                        "messageDescription" => "Sign up success",
                        "messageStatus" => "success",
                        "mobileNo" => $phoneNumber,
                        "titleName" => $patientData['pinitial'],
                        "firstName" => $patientData['pname'],
                        "lastName" => $patientData['psur'],
                        "middleName" => "",
                        "titleNameEn" => $titleNameEn,
                        "middleNameEn" => "",
                        "firstNameEn" => $firstNameEn,
                        "lastNameEn" => $lastNameEn,
                    );
                } else {
                    $result = array(
                        "messageCode" => 20000,
                        "messageDescription" => "เบอร์โทรศัพท์ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อแก้ไขเบอร์โทรศัพท์",
                        "messageStatus" => "fail",
                    );
                }
            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                    "messageStatus" => "fail",
                );
            }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                "messageStatus" => "fail",
            );
        }
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "parameter is incorrect",
            "messageStatus" => "fail",
        );
    }
}

function GetEname($Mydata, $c_sql_his, $hospitalNumber)
{
    $sql = "SELECT replace(name,'    ',' ') AS fullname FROM $Mydata.Ename AS A WHERE A.hn='" . $hospitalNumber . "'";
    $data = $c_sql_his->run_sql_return_array1d($sql);
    if ($data['fullname'] == null) {
        $data['fullname'] = "";
    }
    return $data['fullname'];
}

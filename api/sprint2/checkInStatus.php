<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "language", "appointmentId", "appointmentDate", "appointmentTime", "clinicCode", "question")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        // print_r(json_encode($post));
        $result = array(
            "messageCode" => 10000,
            "messageDescription" => "",
            "messageStatus" => "success",
        );
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}

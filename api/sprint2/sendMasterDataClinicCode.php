<?php
//http://203.154.116.83/ws/rest/receiveMasterDataClinicCode
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}
$c_sql_his = new class_mysql();
$connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

// $setEncode = "SET names tis620";
// $c_sql_his->run_sql($setEncode);
$Mydata = $isProduction? 'Mydata':'DATATEST';
$OPD = $isProduction? 'OPD':'DATATEST';
$sql = "SELECT c_unit as clinicCode ,u_name as clinicName , c_opd as locationCode, concat('Counter ' ,n_opd) as locationName ,'Active' as status
            FROM $Mydata.Funit as A
            INNER JOIN $OPD.Fopdcounter as B on A.counter=B.c_opd";

$data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
$c_sql_his->close_sql($connnect);

if (isset($data)) {
    if (count($data) != 0) {
        $api = "https://mororcare.medicine.psu.ac.th/ws/rest/receiveMasterDataClinicCode";
        $clinicCodeList = array();
        foreach ($data as $k => $v) {
            foreach ($v as $key => $value) {
                if ($v[$key] == null) {
                    $v[$key] = "";
                }
            }
            array_push($clinicCodeList, $v);
        }

        $payload = array(
            "language" => "TH",
            "clinicCodeList" => $clinicCodeList,
        );

        // $cliniccodedata = json_encode($payload);
        // $lines = "";
        // for ($i = 0; $i < 150; $i++) {
        //     $lines .= "=";
        // }

        // $logText = json_encode(array(
        //     "date" => date("Y-m-d H:i:s"),
        //     "data" => json_decode($cliniccodedata, false),
        // ));

        $con = new class_mysql();
        $con->connectSQL($CONFIGS['server']['HOSAPP']);
        $token = new accessToken($con);
        $result = array(
            "date" => date("Y-m-d H:i:s"),
            "token" => $token->get()["accessToken"],
            "cliniccodelist" => $payload,
        );

        $jsonFile = "cliniccode-" . date("Y-m-d") . ".json";
        @file_put_contents(ROOTPATH . "/logs/sent/cliniccode/" . $jsonFile, json_encode($result, JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);

        // //********** Send Data to URL by cURL **********//
        // $ch = curl_init($api);
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        // //attach encoded JSON string to the POST fields
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $cliniccodedata);
        // //set the content type to application/json
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        // //return response instead of outputting
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // //execute the POST request
        // $responseClinicCode = curl_exec($ch);
        // //close cURL resource
        // curl_close($ch);

        // $resArr = (array) json_decode($responseClinicCode, false);

        // foreach ($resArr as $k => $v) {
        //     if ($k === "messageCode") {
        //         $resArr[$k] = (int) $v;
        //     }
        // }
        // $result = $resArr;
        // //********************************************//

    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "",
            "messageStatus" => "fail",
        );
    }
} else {
    $result = array(
        "messageCode" => 20000,
        "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
        "messageStatus" => "fail",
    );
}

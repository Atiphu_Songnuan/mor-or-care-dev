<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}
// header('Content-Type: charset=utf-8');
if ($c_fun->is_body($post, array("hospitalNumber", "language")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        $c_sql_his = new class_mysql();
        $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

        $Mydata = $isProduction? 'Mydata':'DATATEST';
        $sql = "SELECT  concat('$hn', '|', date_ap,'|',code_ap) as appointmentId ,
                        comment as appointmentTitle ,
                        date_ap as appointmentDate ,
                        time_ap as appointmentTime ,
                        'OPD' as appointmentType ,
                        '' as patientType ,
                        code_ap as clinicCode ,
                        unit_ap as clinicName ,
                        '' as unitCode ,
                        '' as unitName ,
                        code_ap as locationCode ,
                        unit_ap as location ,
                        '' as appointmentStatus ,
                        '' as wardCode ,
                        '' as wardName
                FROM    $Mydata.Appoint
                WHERE   hn='$hn'
                AND     date_ap >= curdate()";

        $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
        $c_sql_his->close_sql($connnect);

        if (isset($data)) {
            $appointmentList = array();
            if (count($data) != 0) {
                //Set Phone Number list
                // $testdata = array("074541728","0958567419");
                // $allMedicalWelfares = array();
                foreach ($data as $v) {
                    foreach ($v as $key => $value) {
                        if ($v[$key] == null) {
                            $v[$key] = "";
                        }
                    }
                    array_push($appointmentList, $v);
                    
                }

                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "",
                    "messageStatus" => "success",
                    "appointmentList" => $appointmentList,
                );
            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "ไม่มีข้อมูลนัดหมาย",
                    "messageStatus" => "fail",
                    "appointmentList" => $appointmentList,
                );
            }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                "messageStatus" => "fail",
            );
        }
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}

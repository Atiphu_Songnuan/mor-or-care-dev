<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_body($post, array("hospitalNumber", "language")) && isset($hn)) {
    if ($hn === $post['hospitalNumber']) {
        $c_sql_his = new class_mysql();
        $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

        // $hospitalNumber="0765818";
        $MONEY = $isProduction? 'MONEY':'DATATEST';
        $sql = "SELECT Maininsclname as medicalWelfares, Startdate as effectiveStartDate, Expdate as effectiveEndDate,'' as medicalWelfaresProcessInformation, Hmainopname as payer,'' as medicalWelfaresStatus
                FROM $MONEY.SitWebNhso
                WHERE Hn='$hn'";

        $data = $c_sql_his->run_sql_return_array1d($sql, MYSQLI_ASSOC);

        $c_sql_his->close_sql($connnect);

        if (isset($data)) {
            $medicalWelfaresList = array();
            if (count($data) != 0) {
                // print_r($data);
                //Check value is NULL
                foreach ($data as $k => $v) {
                    if ($data[$k] == null) {
                        $data[$k] = "";
                    }

                    // yyyy-mm-dd
                    if ($k == "effectiveStartDate" || $k == "effectiveEndDate") {
                        if ($data[$k] != null || $data[$k] != "") {
                            if ($data[$k] === "NoExp") {
                                $data[$k] = "";
                            } else {
                                $year = substr($data[$k], 0, 4);
                                $month = substr($data[$k], -4, -2);
                                $day = substr($data[$k], 6);
                                $newDateFormat = strval(intval($year) - 543) . "-" . $month . "-" . $day;
                                $data[$k] = $newDateFormat;
                            }
                        } else {
                            $data[$k] = "";
                        }
                    }
                }

                //Add new data to array
                array_push($medicalWelfaresList, $data);

                $result = array(
                    "messageCode" => 10000,
                    "messageDescription" => "",
                    "messageStatus" => "success",
                    "hospitalNumber" => $hn,
                    "name" => "",
                    "medicalWelfaresList" => $medicalWelfaresList,
                );
            } else {
                $result = array(
                    "messageCode" => 20000,
                    "messageDescription" => "ไม่พบข้อมูลสิทธิ์การรักษา",
                    "messageStatus" => "fail",
                    "hospitalNumber" => $hn,
                    "name" => "",
                    "medicalWelfaresList" => $medicalWelfaresList,
                );
            }
        } else {
            $result = array(
                "messageCode" => 20000,
                "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
                "messageStatus" => "fail",
            );
        }

    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "หมายเลขผู้ป่วย(HN) ของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
            "messageStatus" => "fail",
        );
    }
}

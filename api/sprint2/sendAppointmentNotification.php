<?php
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}
// header('Content-Type: charset=utf-8');

set_time_limit(0);
ini_set('max_execution_time', 0); //0=NOLIMIT
ini_set("memory_limit", "-1");

$his = new class_mysql();
$connect = $his->connectSQL($CONFIGS['server']['HIS']);

// $setEncode = "SET names tis620";
// $c_sql_his->run_sql($setEncode);
$Mydata = $isProduction? 'Mydata':'DATATEST';
$sql = "SELECT  hn as hospitalNumber,
                concat(hn,'|',date_ap,'|',code_ap) as appointmentId,
                comment as appointmentTitle,
                date_ap as appointmentDate,
                time_ap as appointmentTime,
                code_ap as clinicCode,
                unit_ap as clinicName,
                '' as location,
                '' as remark,
                'OPD' as appointmentType,
                '' as appointmentStatus,
                '' as wardCode,
                '' as wardName
        FROM $Mydata.Appoint
        WHERE date_ap = curdate()+interval 3 day
        ";

$data = $his->run_sql_return_array($sql, MYSQLI_ASSOC);
$his->close_sql($connect);

if (isset($data)) {
    if (count($data) != 0) {
        $data = array_map(function ($v) {
            return $v === "" ? null : $v;
        }, $data);

        $appoint_data = array();
        foreach ($data as $k => $v) {
            $payload = array(
                "hospitalNumber" => $v["hospitalNumber"],
                "appointmentId" => $v["appointmentId"],
                "appointmentTitle" => $v["appointmentTitle"],
                "appointmentDate" => $v["appointmentDate"],
                "appointmentTime" => $v["appointmentTime"],
                "clinicCode" => $v["clinicCode"],
                "clinicName" => $v["clinicName"],
                "location" => $v["location"],
                "remark" => $v["remark"],
                "appointmentType" => $v["appointmentType"],
                "appointmentStatus" => $v["appointmentStatus"],
                "wardCode" => $v["wardCode"],
                "wardName" => $v["wardName"],
            );
            array_push($appoint_data, $payload);
            // $appoint_data .= json_encode($payload).PHP_EOL;
        }

        // echo json_encode($appoint_data);
        $con = new class_mysql();
        $con->connectSQL($CONFIGS['server']['HOSAPP']);
        $token = new accessToken($con);
        $result = array(
            "date" => date("Y-m-d H:i:s"),
            "token" => $token->get()["accessToken"],
            "appointmentlist" => $appoint_data,
        );

        $jsonFile = "appointment-" . date("Y-m-d") . ".json";
        @file_put_contents(ROOTPATH . "/logs/sent/appointment/" . $jsonFile, json_encode($result, JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);

        // // $scriptFile = __DIR__ . "/sendAppointmentNotification.py";
        // // sleep(3);

        // // $command = escapeshellcmd("python sendAppointmentNotification.py " .$jsonFile);
        // // // print_r($command);
        // // $output = shell_exec($command);
        // // var_dump($output);
        // // die;

        // $result = array(
        //     "messageCode" => 10000,
        //     "messageDescription" => "",
        //     "messageStatus" => "success",
        // );

        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    } else {
        $result = array(
            "messageCode" => 20000,
            "messageDescription" => "ไม่มีนัดหมาย",
            "messageStatus" => "fail",
        );
    }
} else {
    $result = array(
        "messageCode" => 20000,
        "messageDescription" => "",
        "messageStatus" => "fail",
    );
}

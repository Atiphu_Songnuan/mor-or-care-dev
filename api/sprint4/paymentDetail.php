<?php
// WS007-3-Get Payment Detail
// Update: 06-11-19
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_input($post, array("hospitalNumber", "language", "invoiceId", "station"))) {

    $result = paymentDetail($post, $hn);

}
 
function paymentDetail($post, $hn){
    global $CONFIGS, $isProduction, $c_fun, $authorize;

    if(isset($authorize['stationID'])) $hn = $post['hospitalNumber'];


    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
        );
    }

    $rfu = new restful();
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';
    $invoiceId = $post['invoiceId'];
    $station = $post['station'];
    $citizenId = intval($post['citizenId']);

    //station
    $sql = "SELECT id
            FROM $PAYMENT.station
            WHERE stationName = '$station' ";
    $isStation = $his->run_sql_return_array1d($sql);
    if(empty($isStation)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "จุดชำระเงินไม่ถูกต้อง",
                "Invalid station"
            ),
            "messageStatus" => "fail",
        );
    }

    //invoice
    if(isset($authorize['stationID'])){
        $sql = $rfu->toSqlQuery(array(
            'method' => 'GET',
            'query' => '/invoiceId/'.$invoiceId.'/hospitalNumber/'.$hn.'/citizenId/'.$citizenId,
            'table' => $PAYMENT.'.invoice',
            'limit' => 1
        ));
        $invoice = $his->run_sql_return_array1d($sql);
        if(empty($invoice)){
            return array(
                "messageCode" => 20000,
                "messageDescription" => array(
                    "ไม่พบหมายเลขใบแจ้งยอดชำระในระบบ",
                    "Error invoiceId not found"
                ),
                "messageStatus" => "fail",
            );
        }
    }else{
        $sql = $rfu->toSqlQuery(array(
            'method' => 'GET',
            'query' => '/invoiceId/'.$invoiceId.'/hospitalNumber/'.$hn,
            'table' => $PAYMENT.'.invoice',
            'limit' => 1
        ));
        $invoice = $his->run_sql_return_array1d($sql);
        if(empty($invoice)){
            return array(
                "messageCode" => 20000,
                "messageDescription" => array(
                    "ไม่พบหมายเลขใบแจ้งยอดชำระในระบบ",
                    "Error invoiceId not found"
                ),
                "messageStatus" => "fail",
            );
        }
    }

    $sql = "SELECT A.id
            FROM $PAYMENT.payment AS A
            WHERE A.hospitalNumber = '$hn' && A.invoiceId = '$invoiceId' && isVoid = 0
            LIMIT 1";
    $isPayment = $his->run_sql_return_array1d($sql);
    if(!empty($isPayment)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "ใบแจ้งยอดชำระ $invoiceId ได้รับการชำระเงินแล้ว กรุณาตรวจสอบ",
                "Invoice $invoiceId has been paid"
            ),
            "messageStatus" => "fail",
        );
    }

    //bill
    $sql = $rfu->toSqlQuery(array(
        'method' => 'GET',
        'query' => '/invoice_id/'.$invoice['id'].'/?sort=id_asc',
        'table' => $PAYMENT.'.bill',
        'limit' => 100
    ));
    $bills = $his->run_sql_return_array($sql);
    $billList = array();
    $seq = 1;
    $totalAmountInRight = 0;
    $totalAmountOverRight = 0;
    $totalAmount = 0;
    foreach ($bills as $v) {
        $billList[] = array(
            "billSequenceNo" => "$seq",
            "billName" => $v['billName'],
            "amountInRight" => $v['amountInRight'],
            "amountOverRight" => $v['amountOverRight'],
            "performStatus" => ($v['performStatus']==1)? true:false,          
        );
        $seq++;
        $totalAmountInRight += floatval($v['amountInRight']);
        $totalAmountOverRight += floatval($v['amountOverRight']);
        $totalAmount += floatval($v['amountOverRight']);
    }
    $his->close_sql($connnect);

    //info user
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HIS']);
    $Mydata = $isProduction? 'Mydata':'DATATEST';
    $sql = "SELECT A.pname, A.psur
            FROM $Mydata.Medrec as A
            WHERE A.hn ='$hn'
            LIMIT 1";
    $name = $his->run_sql_return_array1d($sql);
    $his->close_sql($connnect);

    return array(
        "messageCode" => 10000,
        "messageDescription" => "",
        "messageStatus" => "success",
        "hospitalNumber" => "$hn",
        "name" => "$name[pname] $name[psur]",
        "performDate" => $c_fun->echoDatetime($invoice['performDate']),
        "invoiceId" => $invoice['invoiceId'],
        "invoiceDateTime" => $c_fun->echoDatetime($invoice['invoiceDateTime']),
        "eligible" => $invoice['eligible'],
        "billList" => $billList,
        "totalAmountInRight" => number_format($totalAmountInRight, 2, '.', ''),
        "totalAmountOverRight" => number_format($totalAmountOverRight, 2, '.', ''),
        "totalAmount" => number_format($totalAmount, 2, '.', '')
    );

}
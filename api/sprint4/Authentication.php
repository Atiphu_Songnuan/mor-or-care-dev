<?php
// WS008-1-Authentication(KIOSK)
// Update: 06-11-19
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_input($post, array("type")) && isset($authorize['stationID'])) {

    /*die($c_sry->jwt->encode(json_encode(array(
        'stationID' => '002'
    ))));*/
    //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0aW9uSUQiOiIwMDEifQ.Ln416cvL0LlBaOHD-KFF5zwx_AV3onZC54W38sLfs9c

    $result = Authentication($post, $authorize['stationID']);
    
}

function Authentication($post, $stationID){
    global $CONFIGS, $isProduction;

    $hos = new class_mysql();
    //$hos->debug = true;
    $connnect_hos = $hos->connectSQL($CONFIGS['server']['HOSAPP']);

    $his = new class_mysql();
    //$his->debug = true;
    $connnect_his = $his->connectSQL($CONFIGS['server']['HIS']);

    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';

    //station
    $sql = "SELECT id
            FROM $PAYMENT.station
            WHERE id = '$stationID'
            LIMIT 1";
    $isStation = $hos->run_sql_return_array1d($sql);
    if(empty($isStation)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => "Invalid station",
            "messageStatus" => "fail",
        );
    }

    $Mydata = $isProduction? 'Mydata':'DATATEST';
    $BloodBank = $isProduction? 'BloodBank':'DATATEST';
    $sql = "SELECT      medrec.hn,
                        medrec.pinitial ,
                        medrec.pname,
                        medrec.psur,
                        date(medrec.bdate) AS dateOfBirth,
                        (YEAR(NOW()) - YEAR(medrec.bdate)) as age,
                        medrec.rphone,
                        concat(blood.BGrp, blood.RH) as bloodGroup,
                        concat(medrec.paddress, ' ', medrec.rarea) as address
            FROM        $Mydata.Medrec as medrec
            LEFT JOIN   $BloodBank.BB_Patient as blood ON medrec.hn = blood.hn 
            WHERE ";
    
    switch ($post['type']) {

        case 'CITIZENID':
            $citizenId = $post['citizenId'];
            $sql.= "medrec.pphone = '$citizenId' ";
            break;

        case 'INVOICEID':
            $invoiceId = $post['invoiceId'];
            $sql2 = "SELECT hospitalNumber FROM $PAYMENT.invoice WHERE invoiceId = '$invoiceId' LIMIT 1";
            $data = $hos->run_sql_return_array1d($sql2);
            $hn = $data[0];
            $sql.= "medrec.hn = '$hn' ";
            break;

        default:
            //HOSPITALNUMBER
            $hn = $post['hospitalNumber'];
            $sql.= "medrec.hn = '$hn' ";
            break;
    }

    $sql.= "LIMIT 1";
    $data = $his->run_sql_return_array1d($sql);

    $name =  $data['pname'].' '.$data['psur'];
        
    $his->close_sql($connnect_his);
    $hos->close_sql($connnect_hos);

    if(empty($data)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => "ไม่พบข้อมูล กรุณาติดต่องานเวชระเบียน",
            "messageStatus" => "fail",
        );
    }else{
        return array(
            "messageCode" => 10000,
            "messageDescription" => "",
            "messageStatus" => "success",
            "patientProfile" => array(
                'hospitalNumber' => $data['hn'],
                'nameTh' =>  $name,
                'nameEn' =>  $name,
                'age' => intval($data['age']),
            )
        );
    }

}
<?php
// WS007-9-Receive Void Payment
// Update: 06-11-19
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_input($post, array("voidDateTime", "verifyRefID"))) {

    $result = VoidPayment($post);

}

function VoidPayment($post){
    global $CONFIGS, $isProduction, $c_fun;

    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';

    $ctoken = new accessToken($his);
    $data = $ctoken->get();
    $access_token = $data['accessToken'];

    $url = $CONFIGS['api'].'receiveVoidPayment';
    $data = array(
        'voidDateTime' => $c_fun->echoDatetime($post['voidDateTime']),
        'verifyRefID' => $post['verifyRefID']
    );
    $header = array(
        'Authorization: Bearer '.$access_token,
        'User-Agent: Medicine PSU - Void Payment',
        'Content-Type: application/json',
        'lang: TH'
    );
    $context = stream_context_create(
        array(
            'http' => array(
                'method'  => 'POST',
                'header'  => implode("\r\n", $header),
                'content' => json_encode($data),
            ),
            'ssl'   =>  array(
                'verify_peer' => false,
                'verify_peer_name' => false,
            ),
        )
    );
    $response = @file_get_contents($url, false, $context);
    $result = json_decode($response, true);

    if($response === false){
        $result = array(
            "messageCode" => 79002,
            "messageDescription" => "Error sending to SCB Server",
            "messageStatus" => "fail",
        );
    }

    $verifyRefID = $post['verifyRefID'];
    $sql = "UPDATE $PAYMENT.payment SET isVoid = '1' WHERE verifyRefID = '$verifyRefID' ";
    $his->run_sql($sql);
    $his->close_sql($connnect);

    return $result;
}

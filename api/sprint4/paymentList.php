<?php
// WS007-2-Get Payment List
// Update: 06-11-19
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if(isset($authorize['stationID'])) $hn = $post['hospitalNumber'];

if ($c_fun->is_input($post, array("hospitalNumber", "language")) && isset($hn)) {
    
    $result = PaymentList($hn, $post);

}

function PaymentList($hn, $post){
    global $CONFIGS, $isProduction, $c_fun, $authorize;

    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
            "invoiceList" => array(),
        );
    }

    $rfu = new restful();
    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';

    if(isset($authorize['stationID'])){
        $citizenId = intval($post['citizenId']);
        $sql = "SELECT *
                FROM $PAYMENT.invoice AS A
                WHERE (A.hospitalNumber = '$hn' || A.citizenId = '$citizenId') && A.invoiceDateTime >= (NOW() - INTERVAL 24 HOUR)
                ORDER BY A.invoiceDateTime DESC
                LIMIT 100";
    }else{
        $sql = "SELECT *
                FROM $PAYMENT.invoice AS A
                WHERE A.hospitalNumber = '$hn' && A.invoiceDateTime >= (NOW() - INTERVAL 24 HOUR)
                ORDER BY A.invoiceDateTime DESC
                LIMIT 100";
    }
    $data = $his->run_sql_return_array($sql);

    $list = array();
    foreach ($data as $k => $v) {
        $id = $v['id'];
        $invoiceId = $v['invoiceId'];
        $sql = "SELECT A.id
                FROM $PAYMENT.payment AS A
                WHERE A.hospitalNumber = '$hn' && A.invoiceId = '$invoiceId' && A.isVoid = '0'
                LIMIT 1";
        $isPayment = $his->run_sql_return_array1d($sql);
        if(empty($isPayment)){
            $sql = "SELECT SUM(B.amountOverRight) AS totalAmount
                    FROM $PAYMENT.bill AS B 
                    WHERE B.invoice_id = '$id' ";
            $totalAmount = $his->run_sql_return_array1d($sql);
            $list[] = array(
                'invoiceId' => $v['invoiceId'],
                'invoiceDateTime' => $c_fun->echoDatetime($v['invoiceDateTime']),
                'performDate' => $c_fun->echoDatetime($v['performDate']),
                'totalAmount' => $totalAmount['totalAmount'],
                'payable' => ($v['payable']==1 ? true:false),
                'remark' => $v['remark'],
                'doctor' => $v['doctor'],
                'clinic' => $v['clinic'],
                'serviceDeliveryLocationCode' => $v['serviceDeliveryLocationCode'],
                'serviceDeliveryLocationName' => $v['serviceDeliveryLocationName']
            );
        }
    }
    $his->close_sql($connnect);

    if(empty($list)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "ไม่มีข้อมูลรายการที่ต้องชำระ",
                "The invoices already have been paid",
            ),
            "messageStatus" => "fail",
            "invoiceList" => $list,
        );
    }else{
        return array(
            "messageCode" => 10000,
            "messageDescription" => "",
            "messageStatus" => "success",
            "invoiceList" => $list,
        );
    }

}

<?php
/*
    WS007-6-UnLock Payment
    Update: 06-11-19
*/
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_input($post, array("hospitalNumber", "station", "verifyRefID"))) {

    $result = Unlock($hn, $post);

}

function Unlock($hn, $post){
    global $CONFIGS, $isProduction, $authorize;

    if(isset($authorize['stationID'])) $hn = $post['hospitalNumber'];

    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
        );
    }

    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';
    
    //station
    $station = $post['station'];
    $sql = "SELECT id
            FROM $PAYMENT.station
            WHERE stationName = '$station' ";
    $isStation = $his->run_sql_return_array1d($sql);
    if(empty($isStation)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "จุดชำระเงินไม่ถูกต้อง",
                "Invalid station"
            ),
            "messageStatus" => "fail",
        );
    }

    //check lock verify
    $verifyRefID = $post['verifyRefID'];
    $sql = "SELECT invoiceId
            FROM $PAYMENT.lock
            WHERE verifyRefID = '$verifyRefID' ";
    $verify = $his->run_sql_return_array1d($sql);
    if(empty($verify)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => "Invalid verify reference ID",
            "messageStatus" => "fail",
        );
    }

    $sql = "DELETE FROM $PAYMENT.lock WHERE verifyRefID = '$verifyRefID' ";
    $his->run_sql($sql);
    $his->close_sql($connnect);

    return array(
        "messageCode" => 10000,
        "messageDescription" => "",
        "messageStatus" => "success",
    );
}
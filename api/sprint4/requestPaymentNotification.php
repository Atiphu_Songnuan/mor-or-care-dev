<?php
// WS007-1-Receive Payment Noti
// Update: 06-11-19
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_input($post, array("invoice", "billList"))) {

    $result = InsertInvoice();

}

function InsertInvoice(){
    global $CONFIGS, $isProduction;

    $rfu = new restful();
    if($rfu->getMethod() != 'POST') $rfu->return_status(400, true);

    $his = new class_mysql();
    $connnect = $his->connectSQL($CONFIGS['server']['HOSAPP']);
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';

    $data = @file_get_contents('php://input');
    $data = json_decode($data, true);
    $invoiceId = $data['invoice']['invoiceId'];

    $sql = "SELECT id FROM $PAYMENT.invoice WHERE invoiceId = '$invoiceId' LIMIT 1";
    $check = $his->run_sql_return_array1d($sql);
    if(!empty($check)){
        $sql = "DELETE FROM $PAYMENT.invoice WHERE invoiceId = '$invoiceId' LIMIT 1";
        $his->run_sql($sql);
        $sql = "DELETE FROM $PAYMENT.bill WHERE invoice_id = '$check[id]' ";
        $his->run_sql($sql);
    }

    $his->run_sql("BEGIN");
    
    $data['invoice']['payable'] = $data['invoice']['payable']? '1':'0';
    $sql = $rfu->toSqlQuery(array(
        'method' => 'PUT',
        'data' => $data['invoice'],
        'table' => $PAYMENT.'.invoice',
    ));
    
    $his->run_sql($sql);
    $id = $his->insert_id();
    $countList = count($data['billList']);
    
    foreach ($data['billList'] as $v) {
        $v['invoice_id'] = $id;
        $v['performStatus'] = $v['performStatus']? '1':'0';
        $sql = $rfu->toSqlQuery(array(
            'method' => 'PUT',
            'data' => $v,
            'table' => $PAYMENT.'.bill',
        ));
        $his->run_sql($sql);
        $totalAmount += floatval($v['amount']);
    }

    $sql = "SELECT COUNT(id) FROM $PAYMENT.invoice WHERE invoiceId = '$invoiceId' ";
    $check = $his->run_sql_return_array1d($sql);

    $sql = "SELECT COUNT(id) FROM $PAYMENT.bill WHERE invoice_id = '$id' ";
    $check2 = $his->run_sql_return_array1d($sql);

    if(intval($check[0]) == 1 && intval($check2[0]) == $countList){
        $his->run_sql("COMMIT");
    }else{
        $his->run_sql("ROLLBACK");
    }

    $ctoken = new accessToken($his);
    $data = $ctoken->get();
    $access_token = $data['accessToken'];

    return array(
        'status' => empty($access_token)? false:true,
        'access_token' => $access_token
    );

    $his->close_sql($connnect);
}

<?php
//http://203.154.116.83/ws/rest/receiveMasterDataClinicCode
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

$OPD = $isProduction? 'OPD':'DATATEST';
$Mydata = $isProduction? 'Mydata':'DATATEST';
$STAFF = $isProduction? 'STAFF':'DATATEST';
if ($c_fun->is_body($post, array("hospitalNumber", "language", "queueList", "requestType", "fromSystem")) && isset($hn)) {

    if ($post['hospitalNumber'] === $hn && $post['requestType'] === "2" && $post['fromSystem'] === "HosApp") {

        $queues = json_encode($post["queueList"]);
        $queuesDecode = json_decode($queues, true);

        $queueInfoList = array();
        foreach ($queuesDecode as $k => $v) {
            $appointmentTime = $queuesDecode[$k]["appointmentTime"];
            $queueNo = $queuesDecode[$k]["queueNo"];

            $data = getQueue($OPD, $hn, $queueNo);

            if (isset($data)) {
                $codeB = $data[0]["CODE_B"];
                $queueInfo = getQueueInfo($OPD, $hn, $codeB, $queueNo);
                if (count($queueInfo) != 0) {
                    $cDoct = $queueInfo[0]["C_DOCT"];
                    $qTime = $queueInfo[0]["QTIME"];
                    $doctorQueue = getDoctorQueue($OPD, $cDoct, $qTime, $codeB);
                    if (count($doctorQueue) != 0) {
                        $appointData = getAppointmentData($OPD, $Mydata, $qTime, $codeB, $cDoct);
                        // print_r($appointData);
                        if (count($appointData) != 0) {
                            // foreach ($appointData as $k => $v) {
                            $queueCount = $appointData[$k]["QCOUNT"];
                            // }
                            $nq = $doctorQueue[$k3]["DOCCOUNT"] + 2 + $queueCount;

                            if ($nq > 0) {
                                $location = getAppointmentLocation($Mydata, $codeB)[0]["u_name"];
                                $doctorName = getDoctorName($STAFF, $cDoct)[0]["name"];
                                //Replace string change time format from 00.00 to 00:00
                                $qTime = str_replace(".", ":", $qTime);
                                $queueData = array(
                                    "queueNo" => $queueNo,
                                    "currentQueueNo" => "",
                                    "amountQueueForWaiting" => $nq,
                                    "queueSystem" => "Q01", // ให้มาแก้ไขกรณีที่มีคิวประเภทอื่นเข้ามา ณ ตอนนี้ Q01 = คิวพบแพทย์
                                    "locationCode" => $codeB,
                                    "location" => $location,
                                    "moreInformation" => "",
                                    "doctor" => $doctorName,
                                    "queueStatus" => "",
                                    "appointmentTime" => $qTime,
                                    "remark" => "",
                                );
                                array_push($queueInfoList, $queueData);
                            }
                        }
                    }
                }
            }
        }
        if (count($queueInfoList) != 0) {
            $result = array(
                "messageCode" => "10000",
                "messageDescription" => "",
                "messageStatus" => "success",
                "hospitalNumber" => $hn,
                "queueList" => $queueInfoList,
            );
        } else {
            $result = array(
                "messageCode" => "20000",
                "messageDescription" => "",
                "messageStatus" => "fail",
                "hospitalNumber" => $hn,
                "queueList" => array(),
            );
        }
    }
}

function getQueue($OPD, $hn, $queueNo)
{
    global $CONFIGS;
    $c_sql_his = new class_mysql();
    $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);
    $sql = "SELECT  HN,
                    CODE_B,
                    QUEUE
            FROM    $OPD.Opdq
            WHERE   HN='$hn'
            AND QUEUE = '$queueNo'
            AND     DATE_D=curdate()";

    $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
    $c_sql_his->close_sql($connnect);
    return $data;
}

//หาข้อมูล queue นั้นๆ
function getQueueInfo($OPD, $hn, $codeB, $queueNo)
{
    // print_r($hn ." ". $codeB . " ". $queueNo);
    global $CONFIGS;
    $c_sql_his = new class_mysql();
    $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);
    $sql = "SELECT CODE_B,
                C_DOCT,
                A.HN,
                DATE_Q,
                QTIME,
                DOCTTIME,
                OUTTIME,
                QUEUE,
                PTAPP,
                PNAME
            FROM $OPD.Opdq as A
            LEFT JOIN $Mydata.Medrec as B on A.HN=B.HN
            WHERE A.HN='$hn'
            AND CODE_B='$codeB'
            AND DATE_D=curdate()
            AND isnull(OUTTIME)
            -- AND     QTIME = '$appointmentTime'
            AND     QUEUE = '$queueNo'
            ORDER BY QTIME";

    $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
    $c_sql_his->close_sql($connnect);
    return $data;
}

//หาจำนวนคิวของแพทย์คนนั้นๆ
function getDoctorQueue($OPD, $cDoct, $qTime, $codeB)
{
    global $CONFIGS;
    $c_sql_his = new class_mysql();
    $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);
    $sql = "SELECT count(HN) AS DOCCOUNT
            FROM $OPD.Opdq
            WHERE DATE_D>=curdate()
                AND C_DOCT= '$cDoct'
                AND isnull(DOCTTIME)
                AND QTIME < '$qTime'
                AND QTIME not LIKE '%1'
                AND CODE_B='$codeB'";

    $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
    $c_sql_his->close_sql($connnect);
    return $data;
}

function getAppointmentData($OPD, $Mydata, $qTime, $codeB, $cDoct)
{
    global $CONFIGS;
    $c_sql_his = new class_mysql();
    $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);
    $sql = "SELECT count(hn) AS QCOUNT
            FROM $Mydata.Appoint
            WHERE date_ap=curdate()
            AND time_ap < '$qTime'
            AND code_ap='$codeB'
            AND c_doct='$cDoct'
            AND hn not in (
                SELECT hn
                FROM $OPD.Opdq
                where date_d>=curdate()
                AND c_doct='$cDoct'
                AND code_b='$codeB'
            )";

    $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
    $c_sql_his->close_sql($connnect);
    return $data;
}

//หา location
function getAppointmentLocation($Mydata, $codeB)
{
    global $CONFIGS;
    $c_sql_his = new class_mysql();
    $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);
    $sql = "SELECT u_name FROM $Mydata.Funit WHERE c_unit='$codeB'";
    $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
    $c_sql_his->close_sql($connnect);
    return $data;
}

//หาชื่อแพทย์
function getDoctorName($STAFF, $cDoct)
{
    global $CONFIGS;
    $c_sql_his = new class_mysql();
    $connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);
    $sql = "SELECT name FROM $STAFF.Medperson WHERE perid='$cDoct'";
    $data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);
    $c_sql_his->close_sql($connnect);
    return $data;
}

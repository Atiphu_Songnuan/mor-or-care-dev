<?php
/*
    WS007-4-Verify Payment
    Update: 06-11-19
*/
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

if ($c_fun->is_input($post, array("hospitalNumber", "invoiceList", "totalAmount", "stationID"))) {

    $result = Verify($hn, $post);

}

function Verify($hn, $post){
    global $CONFIGS, $isProduction, $authorize;

    if(isset($authorize['stationID'])) $hn = $post['hospitalNumber'];

    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
        );
    }

    $his = new class_mysql();
    $connnect_his = $his->connectSQL($CONFIGS['server']['HIS']);

    $hos = new class_mysql();
    $connnect_hos = $hos->connectSQL($CONFIGS['server']['HOSAPP']);

    $invoiceList = $post['invoiceList'];
    $totalAmount = $post['totalAmount'];
    $station = $post['stationID'];

    $OPD = $isProduction? 'OPD':'DATATEST';
    $OPDMONEY = $isProduction? 'OPDMONEY':'DATATEST';
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';
    
    //gen verifyRefID
    $datetime = date("YmdHis");
    $verifyRefID = str_pad($hn, '0', 7).substr($datetime, 1, strlen($datetime));

    if(!isset($invoiceList[0]['invoiceId'])){
        return array(
            "messageCode" => 20000,
            "messageDescription" => "รายการใบแจ้งยอดชำระที่ส่งมาไม่ถูกต้อง",
            "messageStatus" => "fail",
        );
    }

    //check OPD.Allmoney
    if($isProduction){
        $total = 0;
        foreach ($invoiceList as $v) {
            if(isset($v['invoiceId'])){
                $invoiceId = $v['invoiceId'];

                $sql = "SELECT InvoiceId
                        FROM $OPD.Allmoney
                        WHERE HN = '$hn'
                        AND InvoiceId = '$invoiceId'
                        LIMIT 1";
                $isInvoice = $his->run_sql_return_array1d($sql);
                if(empty($isInvoice)){
                    return array(
                        "messageCode" => 20000,
                        "messageDescription" => "ใบแจ้งยอดชำระ $invoiceId ไม่มีอยู่ในระบบ กรุณาติดต่อการเงิน",
                        "messageStatus" => "fail",
                    );
                }
                
                $sql = "SELECT SUM(MONEY)
                        FROM $OPD.Allmoney
                        WHERE HN = '$hn'
                        AND InvoiceId = '$invoiceId'
                        AND (PAY != 'Y' || isnull(Pay))
                        AND (old_ref != 'DDDD' || isnull(old_ref))";
                $Allmoney = $his->run_sql_return_array1d($sql);
                $amount = $Allmoney[0];
                if($amount == 0){
                    return array(
                        "messageCode" => 20000,
                        "messageDescription" => "ใบแจ้งยอดชำระ $invoiceId ได้รับการชำระเงินแล้ว กรุณาตรวจสอบ (1)",
                        "messageStatus" => "fail",
                    );
                }
                $total += $amount;
            }
        }
        if($total != $totalAmount){
            return array(
                "messageCode" => 20000,
                "messageDescription" => "จำนวนเงินรวมของทุกใบแจ้งยอดชำระไม่ตรงกัน กรุณาทำรายการใหม่อีกครั้ง",
                "messageStatus" => "fail",
            );
        }
    }

    //station
    $sql = "SELECT id
            FROM $PAYMENT.station
            WHERE id = '$station' || stationName = '$station'";
    $isStation = $hos->run_sql_return_array1d($sql);
    if(empty($isStation)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => "จุดชำระเงินไม่ถูกต้อง",
            "messageStatus" => "fail",
        );
    }

    //check PAYMENT.invoice
    $total = 0;
    $roundOff = 0;
    $citizenId = "";
    foreach ($invoiceList as $v) {
        if(isset($v['invoiceId'])){
            $invoiceId = $v['invoiceId'];

            $sql = "SELECT id, citizenId
                    FROM $PAYMENT.invoice
                    WHERE invoiceId = '$invoiceId' && hospitalNumber = '$hn'
                    LIMIT 1";
            $invoice = $hos->run_sql_return_array1d($sql);
            if(empty($invoice)){
                return array(
                    "messageCode" => 20000,
                    "messageDescription" => "ใบแจ้งยอดชำระ $invoiceId ไม่มีอยู่ในระบบ",
                    "messageStatus" => "fail"
                );
            }

            $sql = "SELECT A.id
                    FROM $PAYMENT.payment AS A
                    WHERE A.hospitalNumber = '$hn' && A.invoiceId = '$invoiceId'
                    LIMIT 1";
            $isPayment = $hos->run_sql_return_array1d($sql);
            if(!empty($isPayment)){
                return array(
                    "messageCode" => 20000,
                    "messageDescription" => "ใบแจ้งยอดชำระ $invoiceId ได้รับการชำระเงินแล้ว กรุณาตรวจสอบ (0)",
                    "messageStatus" => "fail",
                );
            }

            if(empty($citizenId)) $citizenId = $invoice['citizenId'];
            $id = $invoice['id'];
            $sql = "SELECT SUM(amountOverRight), SUM(roundOff)
                    FROM $PAYMENT.bill
                    WHERE invoice_id = '$id'";
            $bill = $hos->run_sql_return_array1d($sql);
            $total += floatval($bill[0]);
            $roundOff += floatval($bill[1]);
        }
    }
    if($total != $totalAmount){
        return array(
            "messageCode" => 20000,
            "messageDescription" => "จำนวนเงินรวมของทุกใบแจ้งยอดชำระไม่ตรงกัน กรุณาทำรายการใหม่อีกครั้ง",
            "messageStatus" => "fail"
        );
    }

    $reference2 = $citizenId;

    //time out
    $timeOut = date("Y-m-d H:i:s");
    $sql = "DELETE FROM $PAYMENT.lock WHERE timeOut < '$timeOut' ";
    $hos->run_sql($sql);

    //check PAYMENT.lock
    foreach ($invoiceList as $v) {
        if(isset($v['invoiceId'])){
            $invoiceId = $v['invoiceId'];
            $sql = "SELECT invoiceId
                    FROM $PAYMENT.lock
                    WHERE invoiceId = '$invoiceId' 
                    LIMIT 1";
            $lock = $hos->run_sql_return_array1d($sql);
            if(!empty($lock)){
                return array(
                    "messageCode" => 20000,
                    "messageDescription" => "ใบแจ้งยอดชำระ $invoiceId กำลังดำเนินการทำธุระกรรมอยู่ในขณะนี้ กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                    "messageStatus" => "fail"
                );
            }
        }
    }

    //lock invoiceId
    foreach ($invoiceList as $v) {
        if(isset($v['invoiceId'])){

            $invoiceId = $v['invoiceId'];
            $timeOut = date("Y-m-d H:i:s", time() + (60 * 10));
            $sql = "REPLACE INTO $PAYMENT.lock (invoiceId, verifyRefID) VALUES ('$invoiceId', '$verifyRefID');";
            $hos->run_sql($sql);
            $sql = "UPDATE $PAYMENT.lock SET timeOut = '$timeOut' 
                    WHERE invoiceId = '$invoiceId' && verifyRefID =  '$verifyRefID' 
                    LIMIT 1";
            $hos->run_sql($sql);

            if($isProduction){
                //update OPDMONEY.invoice
                $sql = "UPDATE $OPDMONEY.invoice 
                        SET verifyRefID = '$verifyRefID', station = '$station' 
                        WHERE invoiceId = '$invoiceId' && hn = '$hn' ";
                $his->run_sql($sql);
            }

        }
    }

    $his->close_sql($connnect_his);
    $hos->close_sql($connnect_hos);

    return array(
        "messageCode" => 10000,
        "messageDescription" => "",
        "messageStatus" => "success",
        "verifyStatus" => true,
        "verifyRefID" => $verifyRefID,
        "reference2" => $reference2,
        "netAmount" => number_format(($total - $roundOff), 2, '.', ''),
        "roundOff" => number_format($roundOff, 2, '.', '')
    );

}



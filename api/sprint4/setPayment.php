<?php
/*
    WS007-5-Set Payment
    Update: 06-11-19
*/
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

$result = setPayment($hn, $post);

function setPayment($hn, $post){
    global $CONFIGS, $isProduction, $c_fun, $authorize;

    if(isset($authorize['stationID'])) $hn = $post['hospitalNumber'];
    
    $rfu = new restful();

    $his = new class_mysql();
    $connnect_his = $his->connectSQL($CONFIGS['server']['HIS']);

    $hos = new class_mysql();
    $connnect_hos = $hos->connectSQL($CONFIGS['server']['HOSAPP']);

    $OPD = $isProduction? 'OPD':'DATATEST';
    $OPDMONEY = $isProduction? 'OPDMONEY':'DATATEST';
    $PAYMENT = $isProduction? 'PAYMENT':'PAYMENT_BETA';
    $Mydata = $isProduction? 'Mydata':'DATATEST';

    $hospitalNumber = $post['hospitalNumber'];
    $citizenId = $post['citizenId'];
    $jsonData = json_encode($post);
    $verifyRefID = $post['verifyRefID'];
    $invoiceList = $post['invoiceList'];
    $receiptDetailList = array();
    $invoiceId = $invoiceList[0]['invoiceId'];

    if($post['hospitalNumber'] != $hn){
        return array(
            "messageCode" => 20000,
            "messageDescription" => array(
                "HN ไม่ถูกต้อง กรุณาตรวจสอบและลองใหม่อีกครั้ง",
                "Invalid hospitalNumber"
            ),
            "messageStatus" => "fail",
        );
    }
    
    //check lock verify
    $sql = "SELECT invoiceId
            FROM $PAYMENT.lock
            WHERE invoiceId = '$invoiceId' && verifyRefID = '$verifyRefID' ";
    $verify = $hos->run_sql_return_array1d($sql);
    if(empty($verify)){
        return array(
            "messageCode" => 20000,
            "messageDescription" => "Invalid verify reference ID",
            "messageStatus" => "fail",
        );
    }

    //ins payment
    $sql = "INSERT INTO $PAYMENT.payment (hospitalNumber, invoiceId, jsonData, verifyRefID, datetime, isVoid) 
            VALUES ('$hospitalNumber', '$invoiceId', '$jsonData', '$verifyRefID', NOW(), '0');";
    $hos->run_sql($sql);

    $sql = "SELECT id, billCode, billName, amountInRight, amountOverRight, roundOff, performStatus
            FROM $PAYMENT.bill
            WHERE invoice_id = (SELECT id FROM $PAYMENT.invoice WHERE invoiceId = '$invoiceId' )";
    $bills = $hos->run_sql_return_array($sql);
    $receiptDetailList = array();
    $seq = 1;
    $receiptTotalAmountInRight = 0;
    $receiptTotalAmountOverRight = 0;
    $receiptTotalAmount = 0;
    foreach ($bills as $v) {
        $receiptDetailList[] = array(
            "billSequenceNo" => "$seq",
            "billCode" => $v['billCode'],
            "billName" => $v['billName'],
            "amountInRight" => number_format($v['amountInRight'], 2, '.', ''),
            "amountOverRight" => number_format($v['amountOverRight'], 2, '.', ''),
        );
        $seq++;
        $receiptTotalAmountInRight += floatval($v['amountInRight']);
        $receiptTotalAmountOverRight += floatval($v['amountOverRight']);
        $receiptTotalAmount += floatval($v['amountOverRight']);
        $receiptRoundOff += floatval($v['roundOff']);
    }

    $receiptNo = date("di-hs"); //mock data
    $receiptDateTime = date("Y-m-d H:i:s");
    
    if($isProduction){
        // !!!!! ตรงนี้สำคัญที่สุด !!!!! //
        $his->run_sql("BEGIN");

        //ตรวจสอบว่าเลขที่ใบเสร็จเต็มหรือยัง
        $sql = "SELECT num_no
                FROM $OPDMONEY.Rcp_run 
                WHERE vol_no != '' && PerId = 'SCB' ";
        $col = $his->run_sql_return_array1d($sql);
        if($col[0] == '999'){
            //ใช่
            $sql = "UPDATE $OPDMONEY.Rcp_run SET PerId = 'SCB'
                    WHERE PerId = 'SCB2' ";
            $his->run_sql($sql);
        }

        //หาเลขที่ใบเสร็จ
        $sql = "SELECT vol_no, num_no + 1
                FROM $OPDMONEY.Rcp_run 
                WHERE vol_no != '' && PerId = 'SCB' ";
        $col = $his->run_sql_return_array1d($sql);
        $num_no = $col[1];
        $receiptNo = $col[0] .'-'. $num_no;
        
        //update เลขที่ใบเสร็จที่ได้รับ
        $sql = "UPDATE $OPDMONEY.Rcp_run SET num_no = '$num_no'
                WHERE PerId = 'SCB' ";
        $his->run_sql($sql);

        //Object to String
        $creditReference = json_encode($post['creditReference']);
        $qrReference = json_encode($post['qrReference']);

        //Update OPDMONEY.invoice
        $sql = "UPDATE $OPDMONEY.invoice SET 
                    paymentMethod = '$post[paymentMethod]',
                    creditReference = '$creditReference',
                    qrReference = '$qrReference'
                    receiptNo = '$receiptNo',
                    receiptDateTime = '$receiptDateTime'
                WHERE invoiceId = '$invoiceId' && hn = '$hn' ";
        $his->run_sql($sql);

        //Update OPD.Allmoney
        $sql = "UPDATE $OPD.Allmoney SET Pay = 'Y'
                WHERE invoiceId = '$invoiceId' && hn = '$hn' && (PAY != 'Y' || isnull(Pay))";
        $his->run_sql($sql);

        $his->run_sql("COMMIT");
        // !!!!!!!!!!!!!!! //
    }

    //receipt info
    $sql = "SELECT remark, eligible
            FROM $PAYMENT.invoice
            WHERE invoiceId = '$invoiceId' ";
    $receipt = $hos->run_sql_return_array1d($sql);

    //unlock
    $sql = "DELETE FROM $PAYMENT.lock WHERE verifyRefID = '$verifyRefID' ";
    $hos->run_sql($sql);
    

    //info user
    $sql = "SELECT A.pname, A.psur
            FROM $Mydata.Medrec as A
            WHERE A.hn ='$hn'
            LIMIT 1";
    $data = $his->run_sql_return_array1d($sql);
    $name =  $data['pname'].' '.$data['psur'];

    $his->close_sql($connnect_his);
    $hos->close_sql($connnect_hos);

    return array(
        "messageCode" => 10000,
        "messageDescription" => "",
        "messageStatus" => "success",
        "hospitalNumber" => $post['hospitalNumber'],
        "name" => $name,
        "receiptNo" => $receiptNo,
        "receiptDateTime" => $c_fun->echoDatetime($receiptDateTime),
        "verifyRefID" => $verifyRefID,
        "receiptDetailList" => $receiptDetailList,
        "receiptTotalAmountInRight" => number_format($receiptTotalAmountInRight, 2, '.', ''),
        "receiptTotalAmountOverRight" => number_format($receiptTotalAmountOverRight, 2, '.', ''),
        "receiptTotalAmount" => number_format($receiptTotalAmount, 2, '.', ''),
        "receiptNetAmount" => number_format(($receiptTotalAmount - $receiptRoundOff), 2, '.', ''),
        "receiptRoundOff" => number_format($receiptRoundOff, 2, '.', ''),
        "remark" => $receipt['remark'],
        "eligible" => $receipt['eligible']
    );

}

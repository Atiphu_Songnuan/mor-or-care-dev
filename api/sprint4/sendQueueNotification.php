<?php
//http://203.154.116.83/ws/rest/receiveMasterDataClinicCode
if (!defined('SECURITY')) {header("HTTP/1.1 404 Not Found");die;}

$c_sql_his = new class_mysql();
$connnect = $c_sql_his->connectSQL($CONFIGS['server']['HIS']);

// $setEncode = "SET names tis620";
// $c_sql_his->run_sql($setEncode);
$OPD = $isProduction? 'OPD':'DATATEST';

$sql = "SELECT HN,CODE_B FROM $OPD.Opdq WHERE  DATE_D=curdate()  AND isnull(OUTTIME) ORDER BY HN LIMIT 100";

$data = $c_sql_his->run_sql_return_array($sql, MYSQLI_ASSOC);

$queueList = array();
$queueData = array();

if (isset($data)) {
    foreach ($data as $k => $v) {
        $lastopd = $data[$k]["CODE_B"];
        $iHN = $data[$k]["HN"];
        $currentDate = date("Y-m-d");
        $Mydata = $isProduction? 'Mydata':'DATATEST';
        $STAFF = $isProduction? 'STAFF':'DATATEST';
        $sql2 = "SELECT CODE_B,
                            C_DOCT,
                            A.HN,
                            DATE_Q,
                            QTIME,
                            DOCTTIME,
                            OUTTIME,
                            QUEUE,
                            PTAPP,
                            PNAME
                    FROM $OPD.Opdq as A
                    LEFT JOIN $Mydata.Medrec as B on A.HN=B.HN
                    WHERE A.HN='$iHN'
                        AND CODE_B='$lastopd'
                        AND DATE_D=curdate()
                        AND isnull(OUTTIME)
                    ORDER BY QTIME";

        $data2 = $c_sql_his->run_sql_return_array($sql2, MYSQLI_ASSOC);

        if (isset($data2)) {
            // print_r($data2);
            foreach ($data2 as $k2 => $v2) {
                $cDoct = $data2[$k2]["C_DOCT"];
                $qTime = $data2[$k2]["QTIME"];
                $codeB = $data2[$k2]["CODE_B"];
                $queue = $data2[$k2]["QUEUE"];

                //หาจำนวนคิวของแพทย์คนนั้นๆ
                $sql3 = "SELECT count(HN) AS DOCCOUNT
                        FROM $OPD.Opdq
                        WHERE DATE_D>=curdate()
                            AND C_DOCT= '$cDoct'
                            AND isnull(DOCTTIME)
                            AND QTIME < '$qTime'
                            AND QTIME not LIKE '%1'
                            AND CODE_B='$lastopd'";

                $data3 = $c_sql_his->run_sql_return_array($sql3, MYSQLI_ASSOC);
                if (isset($data3)) {
                    foreach ($data3 as $k3 => $v3) {
                        $sql4 = "SELECT count(hn) AS QCOUNT
                            FROM $Mydata.Appoint
                            WHERE date_ap=curdate()
                            AND time_ap < '$qTime'
                            AND code_ap='$codeB'
                            AND c_doct='$cDoct'
                            AND hn not in (
                                SELECT hn
                                FROM $OPD.Opdq
                                where date_d>=curdate()
                                AND c_doct='$cDoct'
                                AND code_b='$lastopd'
                            )";

                        $data4 = $c_sql_his->run_sql_return_array($sql4, MYSQLI_ASSOC);

                        $queueCount;
                        if (isset($data4)) {
                            foreach ($data4 as $k4 => $v4) {
                                $queueCount = $data4[$k4]["QCOUNT"];
                            }

                            //คิวที่รอ
                            $nq = $data3[$k3]["DOCCOUNT"] + 2 + $queueCount;
                            // print_r("Queue Count: " . $nq . "\n");

                            if ($nq > 0) {
                                // $sql5 = "SELECT u_name FROM Mydata.Funit WHERE c_unit='$codeB'";
                                $sql5 = "SELECT u_name FROM $Mydata.Funit WHERE c_unit='$codeB'";
                                $data5 = $c_sql_his->run_sql_return_array($sql5, MYSQLI_ASSOC);

                                $location = $data5[0]["u_name"];

                                // $sql6 = "SELECT name FROM STAFF.Medperson WHERE perid='$cDoct'";
                                $sql6 = "SELECT name FROM $STAFF.Medperson WHERE perid='$cDoct'";
                                $data6 = $c_sql_his->run_sql_return_array($sql6, MYSQLI_ASSOC);

                                $doctor = $data6[0]["name"];

                                $appointmentId = $iHN . "|" .$currentDate ."|". $codeB;
                                $qTime = str_replace(".", ":", $qTime);
                                $queueData = array(
                                    "messageCode" => "1",
                                    "messageDescription" => "",
                                    "messageStatus" => "success",
                                    "hospitalNumber" => $iHN,
                                    "language" => "TH",
                                    "appointmentId" => $appointmentId,
                                    "appointmentTime" => $qTime,
                                    "queueSystem" => "Q01", // ให้มาแก้ไขกรณีที่มีคิวประเภทอื่นเข้ามา ณ ตอนนี้ Q01 = คิวพบแพทย์
                                    "queueNo" => $queue,
                                    "currentQueueNo" => "",
                                    "amountQueueForWaiting" => strval($nq),
                                    "location" => $location,
                                    "moreInformation" => "",
                                    "remark" => "",
                                    "doctor" => $doctor,
                                    "notificationType" => "",
                                );

                                array_push($queueList, $queueData);
                            }
                        }
                    }
                }
            }
        }
    }
    $c_sql_his->close_sql($connnect);

    $con = new class_mysql();
    $con->connectSQL($CONFIGS['server']['HOSAPP']);
    $token = new accessToken($con);
    $result = array(
        "date" => date("Y-m-d H:i:s"),
        "token" => $token->get()["accessToken"],
        "queuelist" => $queueList,
    );

} else {
    $result = array(
        "messageCode" => 20000,
        "messageDescription" => "ข้อมูลของท่านไม่ถูกต้อง กรุณาติดต่อเวชระเบียนเพื่อตรวจสอบข้อมูล",
        "messageStatus" => "fail",
    );
}